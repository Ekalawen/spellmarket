using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{

    public override void Awake() {
        base.Awake();
        name = "InputManager";
        DontDestroyOnLoad(this);
    }

    public bool GetRestartGame() {
        return Input.GetKeyDown(KeyCode.R);
    }

    public bool GetPauseGame() {
        return Input.GetKeyDown(KeyCode.P);
    }

    public bool GetQuitGame() {
        return Input.GetKeyDown(KeyCode.Escape);
    }
}
