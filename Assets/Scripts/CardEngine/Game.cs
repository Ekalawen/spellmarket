using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Game : Singleton<Game> {

    [Header("Pools")]
    public Pool pool;

    [Header("Zones")]
    public DeckZone deck;
    public DeckZone discard;
    public Zone river;
    public int startNbCardsInRiver = 3;
    public Zone temporaryZone;
    public ZoneFillerOnEndTurn riverFiller;
    public FullZoneFillerWhenEmpty deckFiller;

    [Header("Phases")]
    public PhaseManager phaseManager;
    public PhaseSequencer turnSequencer;
    public ActionManager actionManager;
    public EndGameChecker endGameChecker;

    [Header("Players")]
    public Player player1;
    public Player player2;
    public int startNbCardsInHands = 1;
    public PlayerManager playerManager;

    public virtual void Initialize() {
        pool.Initialize();

        deck.Initialize();
        deck.Add(pool.GetAllCards());
        deck.Shuffle();

        discard.Initialize();
        deckFiller.Initialize();

        river.Initialize();
        deck.DrawMultiple(startNbCardsInRiver).ForEach(c => c.MoveTo(river));
        riverFiller.Initialize();
        temporaryZone.Initialize();

        phaseManager.Initialize();
        turnSequencer.Initialize();
        actionManager.Initialize();

        player1.Initialize(deck.DrawMultiple(startNbCardsInHands));
        player2.Initialize(deck.DrawMultiple(startNbCardsInHands));
        playerManager.Initialize(player1, player2, turnSequencer);

        endGameChecker.Initialize();
    }

    public void Begin() {
        turnSequencer.Begin();
    }
}
