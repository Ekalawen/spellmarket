using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using MyBox;
using System;
using System.Linq;

[Serializable]
public class CounterTurnData {

    public int value;
    public bool hasChanged = false;
    public bool hasIncreased = false;
    public bool hasDecreased = false;

    public CounterTurnData(int value) {
        this.value = value;
    }
}

public class CounterHistory : GameMonoBehaviour {

    protected Counter counter;
    protected List<CounterTurnData> turnDatas;

    public void Initialize(Counter counter) {
        this.counter = counter;
        turnDatas = new List<CounterTurnData>();
        AddNewTurnData();
        game.phaseManager.onEndTurn.AddListener(AddNewTurnData);
        counter.onChange.AddListener(OnChange);
        counter.onIncrease.AddListener(OnIncrease);
        counter.onDecrease.AddListener(OnDecrease);
    }

    protected void OnChange(int arg0) {
        turnDatas.Last().hasChanged = true;
        turnDatas.Last().value = counter.GetValue();
    }

    protected void OnIncrease(int arg0) {
        turnDatas.Last().hasIncreased = true;
        turnDatas.Last().value = counter.GetValue();
    }

    protected void OnDecrease(int arg0) {
        turnDatas.Last().hasDecreased = true;
        turnDatas.Last().value = counter.GetValue();
    }

    protected void AddNewTurnData() {
        turnDatas.Add(new CounterTurnData(counter.GetValue()));
    }

    public CounterTurnData GetCurrentTurn() {
        return turnDatas.Last();
    }

    public CounterTurnData GetLastTurn() {
        return turnDatas[turnDatas.Count - 2];
    }

    public bool HasLastTurn() {
        return turnDatas.Count >= 2;
    }

    public bool HasIncreaseLastTurn() {
        return HasLastTurn() && GetLastTurn().hasIncreased;
    }

    public bool HasDecreaseLastTurn() {
        return HasLastTurn() && GetLastTurn().hasDecreased;
    }
}
