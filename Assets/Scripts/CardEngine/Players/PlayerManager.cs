using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PlayerManager : MonoBehaviour {

    public bool autoSwap = false;

    protected Player player1;
    protected Player player2;
    protected Player currentPlayer;

    [HideInInspector]
    public UnityEvent<Player, Card> onPlay = new UnityEvent<Player, Card>(); // The player who plays the card, the card played
    [HideInInspector]
    public PriorityEvent<Player, Card> onPlayPrio = new PriorityEvent<Player, Card>();

    public void Initialize(Player player1, Player player2, PhaseSequencer turnSequencer) {
        this.player1 = player1;
        this.player2 = player2;
        currentPlayer = player2; // Will be swapped to player1 at the first turn
        if (autoSwap) {
            turnSequencer.onStartSequence.AddListener(Swap);
        }
    }

    public void Swap() {
        SetCurrentPlayer(WaitingPlayer());
    }

    public void SetCurrentPlayer(Player player) {
        Assert.IsTrue(player == player1 || player == player2);
        if(player == currentPlayer) {
            return;
        }
        currentPlayer = player;
        CurrentPlayer().onBeginTurn.Invoke();
        WaitingPlayer().onEndTurn.Invoke(WaitingPlayer());
    }

    public Player CurrentPlayer() {
        return currentPlayer;
    }

    public Player WaitingPlayer() {
        return GetOtherPlayer(currentPlayer);
    }

    public bool IsActivePlayer(Player player) {
        return player == currentPlayer;
    }

    public Player GetOtherPlayer(Player player) {
        return player == player1 ? player2 : player1;
    }
}
