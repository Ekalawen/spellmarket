using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using MyBox;

public class Counter : MonoBehaviour {

    public int startValue = 0;
    public bool hasMinValue = true;
    [ConditionalField("hasMinValue")]
    public int minValue = 0;
    public bool hasMaxValue = true;
    [ConditionalField("hasMaxValue")]
    public int maxValue = 10;

    protected int currentValue;

    [HideInInspector]
    public UnityEvent<int> onIncrease = new UnityEvent<int>(); // int is the variation, always positive
    [HideInInspector]
    public UnityEvent<int> onDecrease = new UnityEvent<int>(); // int is the variation, always positive
    [HideInInspector]
    public UnityEvent<int> onChange = new UnityEvent<int>(); // int is the variation, positive or negative
    [HideInInspector]
    public UnityEvent<int> onIncreaseOverflow = new UnityEvent<int>(); // int is the quantity above the max value

    protected CounterHistory history;
    [HideInInspector]
    public IntModifierManager increaseModifier;
    [HideInInspector]
    public IntModifierManager decreaseModifier;

    public void Initialize() {
        currentValue = startValue;
        increaseModifier = new IntModifierManager(this, null);
        decreaseModifier = new IntModifierManager(this, null);
        history = gameObject.AddComponent<CounterHistory>();
        history.Initialize(this);
    }

    public void Increase(int quantity) {
        Assert.IsTrue(quantity > 0);
        quantity = increaseModifier.ApplyTo(quantity);
        int previousValue = currentValue;
        currentValue += quantity;
        if (hasMaxValue) {
            currentValue = Mathf.Min(currentValue, maxValue);
        }
        int variation = currentValue - previousValue;
        onIncrease.Invoke(quantity);
        onChange.Invoke(quantity);
        if(variation < quantity) {
            onIncreaseOverflow.Invoke(quantity - variation);
        }
    }

    public void Decrease(int quantity) {
        Assert.IsTrue(quantity > 0);
        quantity = decreaseModifier.ApplyTo(quantity);
        int previousValue = currentValue;
        currentValue -= quantity;
        if (hasMinValue) {
            currentValue = Mathf.Max(currentValue, minValue);
        }
        int variation = previousValue - currentValue;
        onDecrease.Invoke(quantity);
        onChange.Invoke(quantity);
    }

    public void Set(int newValue) {
        newValue = Mathf.Clamp(newValue, minValue, maxValue);
        if (newValue == currentValue) {
            return;
        }
        if (newValue < GetValue()) {
            int variation = GetValue() - newValue;
            Decrease(variation);
        } else {
            int variation = newValue - GetValue();
            Increase(variation);
        }
    }

    public int GetValue() {
        return currentValue;
    }

    public CounterHistory GetHistory() {
        return history;
    }

    public bool Compare(Counter other, Effect.Comparator comparator) {
        switch (comparator) {
            case Effect.Comparator.EQUAL:
                return GetValue() == other.GetValue();
            case Effect.Comparator.STRICTLY_GREATER:
                return GetValue() > other.GetValue();
            case Effect.Comparator.GREATER_OR_EQUAL:
                return GetValue() >= other.GetValue();
            case Effect.Comparator.STRICTLY_LESSER:
                return GetValue() < other.GetValue();
            case Effect.Comparator.LESSER_OR_EQUAL:
                return GetValue() <= other.GetValue();
            default:
                return false;
        }
    }
}
