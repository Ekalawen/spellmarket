using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Player : GameMonoBehaviour {

    public Counter health;
    public Counter power;
    public Zone hand;

    [HideInInspector]
    public UnityEvent<int> onDamage = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent<int> onHeal = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent<int> onLifeChange = new UnityEvent<int>(); // int is the variation, positive or negative
    [HideInInspector]
    public UnityEvent<int> onGainPower = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent<int> onLosePower = new UnityEvent<int>();
    [HideInInspector]
    public UnityEvent<int> onChangePower = new UnityEvent<int>(); // int is the variation, positive or negative
    [HideInInspector]
    public UnityEvent onBeginTurn = new UnityEvent();
    [HideInInspector]
    public PriorityEvent<Player> onEndTurn = new PriorityEvent<Player>();
    [HideInInspector]
    public UnityEvent<Player, Card> onPlay = new UnityEvent<Player, Card>();

    protected bool cardsPlayedTwice = false;

    public void Initialize(List<Card> initialCards)
    {
        InitializeCounters();
        hand.Initialize();
        initialCards.ForEach(c => c.MoveTo(hand));
        health.onIncreaseOverflow.AddListener(RedirectHealOverflow);
    }

    protected void RedirectHealOverflow(int overflowQuantity) {
        Player otherPlayer = game.playerManager.GetOtherPlayer(this);
        otherPlayer.health.Decrease(overflowQuantity);
    }

    protected void InitializeCounters() {
        health.Initialize();
        health.onIncrease.AddListener(onHeal.Invoke);
        health.onDecrease.AddListener(onDamage.Invoke);
        health.onChange.AddListener(onLifeChange.Invoke);

        power.Initialize();
        power.onIncrease.AddListener(onGainPower.Invoke);
        power.onDecrease.AddListener(onLosePower.Invoke);
        power.onChange.AddListener(onChangePower.Invoke);
    }

    public bool IsActivePlayer() {
        return game.playerManager.IsActivePlayer(this);
    }

    public TurnSequencer GainTurn(bool controlledByOpponent = false) {
        TurnSequencer turn = game.phaseManager.GetFirstTurnSequenceFor(this);
        return game.turnSequencer.AddPhaseAfterCurrentPhase(turn, controlledByOpponent);
    }

    public void SetCardsPlayedTwice(bool value) {
        cardsPlayedTwice = value;
    }

    public bool AreCardsPlayedTwice() {
        return cardsPlayedTwice;
    }

    public Counter GetCounter(Effect.CounterType counterType) {
        return counterType == Effect.CounterType.HEALTH ? health : power;
    }
}
