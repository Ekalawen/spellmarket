using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public abstract class PhaseEnder : GameMonoBehaviour {

    protected Phase phase;

    public virtual void Initialize(Phase phase) {
        this.phase = phase;
    }

    public void End() {
        if(!phase.CanEnd()) {
            return;
        }
        phase.End();
    }

    public abstract void Stop();

    public void EndWhenNoCardAction() {
        if (!game.actionManager.HasCardAction()) {
            Debug.Log($"InstaEnd {phase} !");
            End();
            return;
        }
        Debug.Log($"End {phase} at next remove cardAction !");
        RemoveEnd(); // To ensure this works when we chain multiple EndWhenNoCardAction :)
        game.actionManager.onCardActionEnd.AddListener(End);
        game.actionManager.onCardActionEnd.AddListener(RemoveEnd);
    }

    protected void RemoveEnd() {
        game.actionManager.onCardActionEnd.RemoveListener(End);
        game.actionManager.onCardActionEnd.RemoveListener(RemoveEnd);
    }
}
