using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PhaseEnderOnAction : PhaseEnder {
    public override void Initialize(Phase phase) {
        base.Initialize(phase);
        foreach(var action in phase.actions) {
            action.onPerform.AddListener(End);
        }
    }

    public override void Stop() {
        foreach(var action in phase.actions) {
            action.onPerform.RemoveListener(End);
        }
    }
}
