using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Phase : GameMonoBehaviour {

    public List<Action> actions;
    public PhaseEnder ender;

    [HideInInspector]
    public PriorityEvent onBegin = new PriorityEvent();
    [HideInInspector]
    public PriorityEvent<Phase> onBeginPhase = new PriorityEvent<Phase>();
    [HideInInspector]
    public PriorityEvent onEnd = new PriorityEvent();
    [HideInInspector]
    public PriorityEvent<Phase> onEndPhase = new PriorityEvent<Phase>();

    protected PhaseManager phaseManager;
    protected Phase followingPhase = null;
    protected bool isActive = false;

    public virtual void Initialize() {
        phaseManager = game.phaseManager;
        phaseManager.RegisterPhase(this);
        foreach (Action action in actions) {
            action.Initialize();
        }
        if (ender != null) {
            ender.Initialize(this);
        }
    }

    public void Begin() {
        if(game.endGameChecker.IsGameOver()) {
            return;
        }
        Assert.IsTrue(CanBegin());
        isActive = true;
        BeginSpecific();
        onBegin.Invoke();
        onBeginPhase.Invoke(this);
        EnableActions();
    }

    protected void EnableActions() {
        foreach(Action action in actions) {
            action.Enable();
        }
    }

    public void End() {
        if(game.endGameChecker.IsGameOver()) {
            return;
        }
        Assert.IsTrue(CanEnd(), $"Phase {this} can't End().");
        isActive = false;
        EndSpecific();
        DisableActions();
        onEnd.Invoke();
        onEndPhase.Invoke(this);
        BeginNextPhase();
    }

    protected void DisableActions() {
        foreach (Action action in actions) {
            action.Disable();
        }
    }

    protected void BeginNextPhase() {
        if (followingPhase == null) {
            return;
        }
        followingPhase.Begin();
    }

    protected virtual void BeginSpecific() {
    }

    protected virtual void EndSpecific() {
    }

    public virtual bool CanBegin() {
        //return !game.endGameChecker.IsGameOver() && !isActive;
        return !isActive;
    }

    public virtual bool CanEnd() {
        //return !game.endGameChecker.IsGameOver() && isActive;
        return isActive;
    }

    public bool IsActive() {
        return isActive;
    }

    public virtual bool CanPerform(Action action) {
        return actions.Contains(action) && action.CanPerform();
    }

    public void Perform(Action action) {
        Assert.IsTrue(CanPerform(action));
        action.Perform();
    }

    public void FollowBy(Phase phase) {
        followingPhase = phase;
    }

    public void FollowByInsertInOrder(Phase phase) {
        Phase nextPhase = followingPhase;
        followingPhase = phase;
        phase.followingPhase = nextPhase;
    }

    public Phase GetFollowingPhase() {
        return followingPhase;
    }
}
