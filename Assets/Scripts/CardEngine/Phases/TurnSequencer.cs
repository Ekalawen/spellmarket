using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class TurnSequencer : PhaseSequencer {

    public Player player;

    protected PlayerManager swapper;
    protected bool controlledByOpponent = false;

    public override void Initialize() {
        base.Initialize();
        swapper = Game.Instance.playerManager;
        onBegin.AddListener(game.phaseManager.onBeginTurn.Invoke);
        onEnd.AddListener(game.phaseManager.onEndTurn.Invoke);
    }

    protected override void BeginSpecific() {
        swapper.SetCurrentPlayer(player);
        game.phaseManager.onBeforeBeginTurn.Invoke();
        base.BeginSpecific();
    }

    public void SetControlledByOpponent(bool controlledByOpponent) {
        this.controlledByOpponent = controlledByOpponent;
    }

    public bool IsControlledByOpponent() {
        return controlledByOpponent;
    }
}
