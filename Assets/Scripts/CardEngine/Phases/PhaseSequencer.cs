using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PhaseSequencer : Phase {

    public bool shouldCycle = false;
    [ConditionalField("shouldCycle")]
    public bool shouldResetAfterEachCycle = false;
    public List<Phase> phases;

    [HideInInspector]
    public UnityEvent onStartSequence = new UnityEvent();
    protected Phase currentPhase = null;
    protected List<Phase> initialPhases;

    public override void Initialize()
    {
        base.Initialize();
        if (shouldResetAfterEachCycle) {
            InitializeInitialPhases();
            DuplicatePhases();
        }
        InitializePhases();
    }

    protected void InitializeInitialPhases() {
        initialPhases = phases.Select(p => p).ToList();
        phases.Clear();
    }

    protected void DuplicatePhases() {
        foreach(Phase phase in phases) {
            Destroy(phase.gameObject);
        }
        phases.Clear();
        foreach (Phase initialPhase in initialPhases) {
            Phase newPhase = Instantiate(initialPhase.gameObject, parent: transform).GetComponent<Phase>();
            phases.Add(newPhase);
        }
    }

    protected void InitializePhases() {
        foreach (Phase phase in phases) {
            phase.Initialize();
            phase.onBeginPhase.AddListener(SetCurrentPhase);
            if(phase is TurnSequencer) {
                game.phaseManager.onCreateTurn.Invoke(phase as TurnSequencer, false);
            }
        }
        LinkAllPhases();
        phases.First().onBegin.AddListener(onStartSequence.Invoke);
    }

    protected void SetCurrentPhase(Phase phase) {
        currentPhase = phase;
    }

    protected override void BeginSpecific() {
        phases.First().Begin();
    }

    protected void LinkAllPhases() {
        for(int i = 0; i < phases.Count - 1; i++) {
            Phase current = phases[i];
            Phase next = phases[i + 1];
            current.FollowBy(next);
        }
        if (shouldCycle) {
            phases.Last().onEnd.AddListener(ResetCycle);
        } else {
            phases.Last().onEnd.AddListener(End);
        }
    }

    protected override void EndSpecific() {
        base.EndSpecific();
    }

    protected void ResetCycle() {
        if (shouldResetAfterEachCycle) {
            DuplicatePhases();
            InitializePhases();
        }
        phases.First().Begin();
    }

    public override bool CanBegin() {
        return base.CanBegin() && phases.Count > 0 && phases.First().CanBegin();
    }

    public override bool CanEnd() {
        return base.CanEnd() && !phases.Last().IsActive();
    }

    public Phase CurrentPhase() {
        return currentPhase;
    }

    public TurnSequencer AddPhaseAfterCurrentPhase(TurnSequencer turn, bool controlledByOpponent = false) {
        Assert.IsTrue(shouldCycle && shouldResetAfterEachCycle);
        Assert.IsTrue(phases.Contains(turn));
        if (shouldCycle) {
            phases.Last().onEnd.RemoveListener(ResetCycle);
        } else {
            phases.Last().onEnd.RemoveListener(End);
        }
        Phase initialPhase = initialPhases.Find(initial => (initial as TurnSequencer).player == turn.player);
        Phase newPhase = Instantiate(initialPhase.gameObject, parent: transform).GetComponent<Phase>();
        newPhase.Initialize();
        newPhase.onBeginPhase.AddListener(SetCurrentPhase);
        int currentPhaseIndex = phases.IndexOf(CurrentPhase());
        phases.Insert(currentPhaseIndex + 1, newPhase);
        LinkAllPhases();
        TurnSequencer newTurn = newPhase as TurnSequencer;
        newTurn.SetControlledByOpponent(controlledByOpponent);
        game.phaseManager.onCreateTurn.Invoke(newTurn, true);
        return newTurn;
    }
}
