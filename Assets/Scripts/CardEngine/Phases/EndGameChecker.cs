using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class EndGameChecker : GameMonoBehaviour {

    [HideInInspector]
    public UnityEvent<Player> onWin = new UnityEvent<Player>();
    [HideInInspector]
    public UnityEvent onTie = new UnityEvent();
    [HideInInspector]
    public UnityEvent onGameBegin = new UnityEvent();

    protected Player player1;
    protected Player player2;
    protected bool isGameStarted = false;
    protected bool isGameOver = false;

    public void Initialize() {
        player1 = game.player1;
        player2 = game.player2;
        game.turnSequencer.onBegin.AddListener(OnGameStart);
        game.phaseManager.onEndTurn.AddListener(CheckForEndGame);
        game.phaseManager.GetFirstTurnSequenceFor(player1).phases.First().onBegin.AddListener(CheckForEndGame);
        game.phaseManager.GetFirstTurnSequenceFor(player2).phases.First().onBegin.AddListener(CheckForEndGame);
    }

    protected void OnGameStart() {
        isGameStarted = true;
        onGameBegin.Invoke();
    }

    public bool IsGameStarted() {
        return isGameStarted;
    }

    public void CheckForEndGame() {
        if (HasWin(player1) && HasWin(player2)) {
            WinWithTie();
        } else if (HasWin(player1)) {
            Win(player1);
        } else if (HasWin(player2)) {
            Win(player2);
        }
    }

    protected void WinWithTie() {
        if (GetScore(player1) == GetScore(player2)) {
            Tie();
            return;
        }
        Win(AheadPlayer());
    }

    protected void Win(Player player) {
        isGameOver = true;
        onWin.Invoke(player);
    }

    protected void Tie() {
        isGameOver = true;
        onTie.Invoke();
    }

    public bool HasWin(Player player) {
        Player opponent = game.playerManager.GetOtherPlayer(player);
        return player.power.GetValue() >= player.power.maxValue
            || opponent.health.GetValue() <= 0;
    }

    public int GetScore(Player player) {
        return player.power.GetValue() + player.health.GetValue();
    }

    public Player AheadPlayer() {
        return GetScore(player1) > GetScore(player2) ? player1 : player2;
    }

    public bool IsGameOver() {
        return isGameOver;
    }
}
