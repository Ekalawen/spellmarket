using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class PhaseManager : GameMonoBehaviour {

    protected List<Phase> phases;

    [HideInInspector]
    public UnityEvent<Phase> onBeginPhase = new UnityEvent<Phase>();
    [HideInInspector]
    public UnityEvent<Phase> onEndPhase = new UnityEvent<Phase>();
    [HideInInspector]
    public UnityEvent onBeginTurn = new UnityEvent();
    [HideInInspector]
    public UnityEvent onBeforeBeginTurn = new UnityEvent();
    [HideInInspector]
    public UnityEvent onEndTurn = new UnityEvent();
    [HideInInspector]
    public UnityEvent<TurnSequencer, bool> onCreateTurn = new UnityEvent<TurnSequencer, bool>(); // True if is additionnal turn ! :)

    public void Initialize() {
        phases = new List<Phase>();
    }

    public void RegisterPhase(Phase phase) {
        if(phases.Contains(phase)) {
            return;
        }
        phases.Add(phase);
        phase.onBeginPhase.AddListener(OnBeginPhase);
        phase.onEndPhase.AddListener(OnEndPhase);
    }

    private void OnEndPhase(Phase phase) {
        onEndPhase.Invoke(phase);
    }

    protected void OnBeginPhase(Phase phase) {
        onBeginPhase.Invoke(phase); 
    }

    public TurnSequencer GetFirstTurnSequenceFor(Player player) {
        return game.turnSequencer.phases.Find(phase => ((TurnSequencer)phase).player == player) as TurnSequencer;
    }
}
