using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Pool : MonoBehaviour {

    [SerializeField]
    protected List<GameObject> cardPrefabs;
    protected List<Card> instantiatedCards;

    [HideInInspector]
    public UnityEvent<Card> onInstantiateCard = new UnityEvent<Card>();

    public void Initialize() {
        instantiatedCards = new List<Card>();
    }

    protected Card InstantiateCard(GameObject cardPrefab) {
        Card card = Instantiate(cardPrefab, parent: transform).GetComponent<Card>();
        card.Initialize();
        instantiatedCards.Add(card);
        onInstantiateCard.Invoke(card);
        return card;
    }

    public Card GetCard() {
        GameObject prefab = MathTools.ChoseOne(cardPrefabs);
        return InstantiateCard(prefab);
    }

    public List<Card> GetAllCards() {
        List<Card> cards = new List<Card>();
        foreach (GameObject prefab in cardPrefabs) {
            cards.Add(InstantiateCard(prefab));
        }
        return cards;
    }

    public List<Card> InstantiatedCards() {
        return instantiatedCards.Select(c => c).ToList();
    }
}
