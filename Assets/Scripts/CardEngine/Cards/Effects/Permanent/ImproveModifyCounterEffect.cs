using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class ImproveModifyCounterEffect : ModifyCounterEffect {

    public int quantity = 1;

    protected override IntModifier CreateModifier() {
        return new IntModifier(TModifierMode.ADD, TModifierPeriod.INFINITE, quantity);
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        ImproveModifyCounterEffect otherEffect = (ImproveModifyCounterEffect)other;
        quantity = otherEffect.quantity;
    }
}
