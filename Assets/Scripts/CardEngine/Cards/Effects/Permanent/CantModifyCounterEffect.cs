using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class CantModifyCounterEffect : ModifyCounterEffect {

    protected override IntModifier CreateModifier() {
        return new IntModifier(TModifierMode.MULTIPLY, TModifierPeriod.INFINITE, 0);
    }
}
