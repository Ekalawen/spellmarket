using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class CopyCounterEffect : Effect {

    public Target target;
    public CounterType counterType;
    public CounterDirection comparator;

    protected Dictionary<UnityEvent<int>, UnityAction<int>> functions;

    public override void Trigger() {
        Stop();
        List<Tuple<Counter, Counter>> counters = GetCountersWithOppositeCounters();
        functions = new Dictionary<UnityEvent<int>, UnityAction<int>>();
        foreach(var counterCouple in counters) {
            Counter counter = counterCouple.Item1;
            Counter oppositeCounter = counterCouple.Item2;
            UnityEvent<int> onCounterVariation = comparator == CounterDirection.GONE_UP ? counter.onIncrease : counter.onDecrease;
            UnityAction<int> function = comparator == CounterDirection.GONE_UP ?
                (variation) => oppositeCounter.Increase(variation) :
                (variation) => oppositeCounter.Decrease(variation);
            functions[onCounterVariation] = function;
            onCounterVariation.AddListener(function);
            onCounterVariation.AddListener(InvokeOnChange);
        }
    }

    public override void Stop() {
        if(functions == null) {
            return;
        }
        base.Stop();
        foreach (var couple in functions) {
            UnityEvent<int> onCounterVariation = couple.Key;
            UnityAction<int> function = couple.Value;
            onCounterVariation.RemoveListener(function);
            onCounterVariation.RemoveListener(InvokeOnChange);
        }
    }

    protected List<Tuple<Counter, Counter>> GetCountersWithOppositeCounters() {
        List<Player> players = GetTargets(target);
        List<Tuple<Counter, Counter>> counters = new List<Tuple<Counter, Counter>>();
        foreach (Player player in players) {
            Counter counter = counterType == CounterType.HEALTH ? player.health : player.power;
            Player oppositePlayer = game.playerManager.GetOtherPlayer(player);
            Counter oppositeCounter = counterType == CounterType.HEALTH ? oppositePlayer.health : oppositePlayer.power;
            counters.Add(new Tuple<Counter, Counter>(counter, oppositeCounter));
        }
        return counters;
    }

    protected void InvokeOnChange(int variation) {
        card.onNotPlayTriggered.Invoke();
    }

    public override void Copy(Effect other) {
        CopyCounterEffect otherEffect = (CopyCounterEffect)other;
        target = otherEffect.target;
        counterType = otherEffect.counterType;
        comparator = otherEffect.comparator;
        functions = otherEffect.functions;
    }
}
