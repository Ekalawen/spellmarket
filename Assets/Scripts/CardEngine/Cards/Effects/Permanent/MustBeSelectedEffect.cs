using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class MustBeSelectedEffect : Effect {

    public bool startValue = true;
    public bool mustBeTaken = true;
    public bool mustBePlayed = true;

    protected bool currentValue;

    public override void Initialize(Card card) {
        base.Initialize(card);
        currentValue = startValue;
    }

    public override void Trigger() {
        currentValue = true;
    }

    public override void Stop() {
        base.Stop();
        currentValue = false;
    }

    public override bool CardMustBeSelected() {
        return currentValue && mustBeTaken;
    }

    public override bool CardMustBePlayed() {
        return currentValue && mustBePlayed;
    }

    public override void Copy(Effect other) {
        MustBeSelectedEffect otherEffect = (MustBeSelectedEffect)other;
        startValue = otherEffect.startValue;
        mustBeTaken = otherEffect.mustBeTaken;
        mustBePlayed = otherEffect.mustBePlayed;
        currentValue = otherEffect.currentValue;
    }
}
