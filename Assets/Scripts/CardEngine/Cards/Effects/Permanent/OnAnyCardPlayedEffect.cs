using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class OnAnyCardPlayedEffect : EffectWithSubEffect {

    public bool onlyOtherCards = true;
    public bool onlyHasBeenSelectedThisTurn = false;

    public override void Trigger() {
        game.playerManager.onPlay.AddListener(OnPlay);
    }

    public override void Stop() {
        base.Stop();
        game.playerManager.onPlay.RemoveListener(OnPlay);
    }

    protected void OnPlay(Player player, Card playedCard) {
        if (onlyHasBeenSelectedThisTurn && !playedCard.HasBeenSelectedThisTurn()) {
            return;
        }
        if(onlyOtherCards && playedCard == card) {
            return;
        }
        subEffect.Trigger();
        card.onNotPlayTriggered.Invoke();
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        OnAnyCardPlayedEffect otherEffect = (OnAnyCardPlayedEffect)other;
        onlyOtherCards = otherEffect.onlyOtherCards;
        onlyHasBeenSelectedThisTurn = otherEffect.onlyHasBeenSelectedThisTurn;
    }
}
