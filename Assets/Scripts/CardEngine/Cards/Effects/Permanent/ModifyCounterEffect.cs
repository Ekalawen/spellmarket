using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public abstract class ModifyCounterEffect : Effect {

    public Target target;
    public CounterType counterType;
    public CounterDirection comparator;

    protected Dictionary<Counter, TModifier<int>> modifiers;

    public override void Trigger() {
        List<Counter> counters = GetCounters();
        modifiers = new Dictionary<Counter, TModifier<int>>();
        foreach(Counter counter in counters) {
            IntModifierManager counterModifier = comparator == CounterDirection.GONE_UP ? counter.increaseModifier : counter.decreaseModifier;
            modifiers[counter] = counterModifier.AddModifier(CreateModifier());
            UnityEvent<int> unityEvent = comparator == CounterDirection.GONE_UP ? counter.onIncrease : counter.onDecrease;
            unityEvent.AddListener(InvokeOnChange);
        }
    }

    protected abstract IntModifier CreateModifier();

    public override void Stop() {
        base.Stop();
        foreach(var couple in modifiers) {
            Counter counter = couple.Key;
            TModifier<int> modifier = couple.Value;
            IntModifierManager counterModifier = comparator == CounterDirection.GONE_UP ? counter.increaseModifier : counter.decreaseModifier;
            counterModifier.RemoveModifier(modifier);
            UnityEvent<int> unityEvent = comparator == CounterDirection.GONE_UP ? counter.onIncrease : counter.onDecrease;
            unityEvent.RemoveListener(InvokeOnChange);
        }
    }

    protected List<Counter> GetCounters() {
        List<Player> players = GetTargets(target);
        return players.Select(p => counterType == CounterType.HEALTH ? p.health : p.power).ToList();
    }

    protected void InvokeOnChange(int variation) {
        card.onNotPlayTriggered.Invoke();
    }

    public override void Copy(Effect other) {
        ModifyCounterEffect otherEffect = (ModifyCounterEffect)other;
        target = otherEffect.target;
        counterType = otherEffect.counterType;
        comparator = otherEffect.comparator;
        modifiers = otherEffect.modifiers;
    }
}
