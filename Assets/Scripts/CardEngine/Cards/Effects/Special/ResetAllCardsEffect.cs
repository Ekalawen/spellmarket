using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class ResetAllCardsEffect : Effect {

    public override void Trigger() {
        ResetZone(game.river);
        ResetHandZone(game.player1.hand);
        ResetHandZone(game.player2.hand);
    }

    protected void ResetHandZone(Zone hand) {
        foreach(Card card in hand.GetAllCards()) {
            if(card == GetCard()) {
                continue;
            }
            card.MoveTo(game.discard);
            game.deck.Draw().MoveTo(hand);
        }
    }

    protected void ResetZone(Zone zone) {
        zone.GetAllCards().ForEach(card => card.MoveTo(game.discard));
    }

    public override void Copy(Effect other) {
    }
}
