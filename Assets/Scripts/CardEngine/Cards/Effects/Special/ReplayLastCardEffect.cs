using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class ReplayLastCardEffect : Effect {

    public override void Trigger() {
        Card lastCardPlayed = GetLastCardPlayed();
        lastCardPlayed.Play();
        //lastCardPlayed.MoveTo(game.discard);
    }

    protected Card GetLastCardPlayed() {
        if(game.discard.Count > 0) {
            return game.discard.GetAllCards().Last();
        }
        return game.deck.GetAllCards().Last();
    }

    public override void Copy(Effect other) {
    }
}
