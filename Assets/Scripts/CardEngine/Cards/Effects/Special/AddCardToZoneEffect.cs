using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class AddCardToZoneEffect : Effect {

    public ZoneType zoneType;

    public override void Trigger() {
        GetZones(zoneType).ForEach(AddCardToZone);
    }

    protected void AddCardToZone(Zone zone) {
        Card card = game.deck.Draw();
        card.MoveTo(zone, isPlayMove: true);
    }

    public override void Copy(Effect other) {
        AddCardToZoneEffect addEffect = other as AddCardToZoneEffect;
        zoneType = addEffect.zoneType;
    }
}
