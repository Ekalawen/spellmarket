using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class SwapAllCardsEffect : Effect {

    public override void Trigger() {
        List<Card> cardsPlayer1 = game.player1.hand.GetAllCards().FindAll(c => c != card);
        List<Card> cardsPlayer2 = game.player2.hand.GetAllCards().FindAll(c => c != card);
        Assert.IsTrue(cardsPlayer1.Count == cardsPlayer2.Count);
        for (int i = 0; i < cardsPlayer1.Count; i++) {
            SwapCards(cardsPlayer1[i], cardsPlayer2[i]);
        }
    }

    public void SwapCards(Card card1, Card card2) {
        Zone card1Zone = card1.CurrentZone();
        Zone card2Zone = card2.CurrentZone();
        card1.MoveTo(card2Zone);
        card2.MoveTo(card1Zone);
    }

    public override void Copy(Effect other) {
    }
}
