using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class ReplayEffect : Effect {

    public Target target;

    public override void Trigger() {
        GetTargets(target).ForEach(player => player.GainTurn());
    }

    public override void Copy(Effect other) {
        ReplayEffect otherEffect = (ReplayEffect)other;
        target = otherEffect.target;
    }
}
