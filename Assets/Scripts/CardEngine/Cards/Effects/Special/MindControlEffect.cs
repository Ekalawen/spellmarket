using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class MindControlEffect : Effect {

    public override void Trigger() {
        Player opponent = GetTargets(Target.OPPONENT).First();
        Player self = GetTargets(Target.SELF).First();
        self.GainTurn();
        TurnSequencer opponentTurn = opponent.GainTurn(controlledByOpponent: true);
    }

    public override void Copy(Effect other) {
    }
}
