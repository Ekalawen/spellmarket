using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayCardsTwiceEffect : Effect {

    public override void Trigger() {
        Player player = game.playerManager.CurrentPlayer();
        player.SetCardsPlayedTwice(true);
    }

    public override void Stop() {
        base.Stop();
        Player player = game.playerManager.CurrentPlayer();
        player.SetCardsPlayedTwice(false);
    }

    public override void Copy(Effect other) {
    }
}
