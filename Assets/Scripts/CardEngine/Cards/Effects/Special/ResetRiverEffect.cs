using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class ResetRiverEffect : Effect {

    public override void Trigger() {
        List<Card> riverCards = game.river.GetAllCards();
        foreach (Card card in riverCards) {
            card.MoveTo(game.discard);
        }
    }

    public override void Copy(Effect other) {
    }
}
