using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayNextCardTwiceEffect : Effect {

    public override void Trigger() {
        Player player = game.playerManager.CurrentPlayer();
        player.SetCardsPlayedTwice(true);
        player.onEndTurn.AddListener(RemovePlayTwiceOnNextPlay);
    }

    protected void RemovePlayTwiceOnNextPlay(Player player) {
        // Need to pass by end of turn to avoid instantly removing the effect with the onPlay being called just after :)
        player.onEndTurn.RemoveListener(RemovePlayTwiceOnNextPlay);
        player.onPlay.AddListener(RemoveCardsPlayedTwice);
    }

    protected void RemoveCardsPlayedTwice(Player player, Card _) {
        player.onPlay.RemoveListener(RemoveCardsPlayedTwice);
        player.SetCardsPlayedTwice(false);
    }

    public override void Copy(Effect other) {
    }
}
