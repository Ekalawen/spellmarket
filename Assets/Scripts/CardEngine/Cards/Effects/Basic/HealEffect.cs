using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class HealEffect : BasicEffect {

    public override void Trigger() {
        GetTargets().ForEach(t => t.health.Increase(quantity));
    }
}
