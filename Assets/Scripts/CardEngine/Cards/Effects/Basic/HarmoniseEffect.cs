using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class HarmoniseEffect : Effect {

    public Target baseTarget;
    public CounterType baseCounterType;
    public Comparator baseComparator;

    public Target harmoniseTarget;
    public CounterType harmoniseCounterType;
    public CounterDirection harmoniseDirection;

    public override void Initialize(Card card) {
        base.Initialize(card);
        Assert.IsTrue(baseTarget != Target.BOTH);
        Assert.IsTrue(harmoniseTarget != Target.BOTH);
        Assert.IsTrue(baseComparator == Comparator.STRICTLY_LESSER || baseComparator == Comparator.STRICTLY_GREATER);
    }

    public override void Trigger() {
        Player player = GetSingleTarget(baseTarget);
        Counter playerCounter = player.GetCounter(baseCounterType);
        Player opponent = game.playerManager.GetOtherPlayer(player);
        Counter opponentCounter = opponent.GetCounter(baseCounterType);

        if(!playerCounter.Compare(opponentCounter, baseComparator)) {
            return;
        }

        int variation = Mathf.Abs(playerCounter.GetValue() - opponentCounter.GetValue());
        playerCounter.Set(opponentCounter.GetValue());

        Player harmonisePlayer = GetSingleTarget(harmoniseTarget);
        Counter harmoniseCounter = harmonisePlayer.GetCounter(harmoniseCounterType);
        if(harmoniseDirection == CounterDirection.GONE_UP) {
            harmoniseCounter.Increase(variation);
        } else {
            harmoniseCounter.Decrease(variation);
        }
    }

    public override void Copy(Effect other) {
        HarmoniseEffect otherEffect = (HarmoniseEffect)other;
        baseTarget = otherEffect.baseTarget;
        baseCounterType = otherEffect.baseCounterType;
        baseComparator = otherEffect.baseComparator;
        harmoniseTarget = otherEffect.harmoniseTarget;
        harmoniseCounterType = otherEffect.harmoniseCounterType;
        harmoniseDirection = otherEffect.harmoniseDirection;
    }
}
