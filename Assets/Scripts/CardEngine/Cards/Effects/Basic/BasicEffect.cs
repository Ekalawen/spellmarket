using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class BasicEffect : Effect
{

    public int quantity = 1;
    public Target target;

    public List<Player> GetTargets() {
        return GetTargets(target);
    }

    public override void Copy(Effect other) {
        BasicEffect otherEffect = (BasicEffect)other;
        quantity = otherEffect.quantity;
        target = otherEffect.target;
    }
}
