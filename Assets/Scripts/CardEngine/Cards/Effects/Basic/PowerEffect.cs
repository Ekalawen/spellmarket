using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PowerEffect : BasicEffect {

    public override void Trigger() {
        GetTargets().ForEach(t => t.power.Increase(quantity));
    }
}
