using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class OnBeginTurnEffect : EffectWithSubEffect {

    public override void OnControlledBy(Player player, Zone previousZone) {
        base.OnControlledBy(player, previousZone);
        player.onBeginTurn.AddListener(subEffect.Trigger);
        player.onBeginTurn.AddListener(card.onNotPlayTriggered.Invoke);
    }

    public override void OnStopControlledBy(Player player, Zone newZone) {
        base.OnStopControlledBy(player, newZone);
        player.onBeginTurn.RemoveListener(subEffect.Trigger);
        player.onBeginTurn.RemoveListener(card.onNotPlayTriggered.Invoke);
    }
}
