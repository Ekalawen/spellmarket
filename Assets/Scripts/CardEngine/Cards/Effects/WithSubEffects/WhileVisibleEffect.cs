using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class WhileVisibleEffect : EffectWithSubEffect {

    public override void OnVisible() {
        base.OnVisible();
        subEffect.Trigger();
    }

    public override void OnStopVisible() {
        base.OnStopVisible();
        subEffect.Stop();
    }
}
