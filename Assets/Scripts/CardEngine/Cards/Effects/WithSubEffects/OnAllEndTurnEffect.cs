using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class OnAllEndTurnEffect : EffectWithSubEffect {

    public override void Initialize(Card card) {
        base.Initialize(card);
        game.phaseManager.onEndTurn.AddListener(subEffect.Trigger);
    }

}
