using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class CounterChangeLastTurnConditionalEffect : ConditionalEffect {

    public Target target;
    public CounterType counter;
    public CounterDirection comparator;

    public override bool ConditionValidated() {
        List<Player> players = GetTargets(target);
        return players.All(ConditionValidatedForPlayer);
    }

    protected bool ConditionValidatedForPlayer(Player player) {
        Counter consideredCounter = counter == CounterType.HEALTH ? player.health : player.power;
        return comparator == CounterDirection.GONE_UP ? consideredCounter.GetHistory().HasIncreaseLastTurn() : consideredCounter.GetHistory().HasDecreaseLastTurn();
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        CounterChangeLastTurnConditionalEffect otherEffect = (CounterChangeLastTurnConditionalEffect)other;
        target = otherEffect.target;
        counter = otherEffect.counter;
        comparator = otherEffect.comparator;
    }

}
