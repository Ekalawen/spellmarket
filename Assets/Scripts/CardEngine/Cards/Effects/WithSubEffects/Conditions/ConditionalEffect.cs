using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class ConditionalEffect : EffectWithSubEffect {

    public override void Trigger() {
        if (ConditionValidated()) {
            subEffect.Trigger();
        } else {
            subEffect.Stop();
        }
    }

    public abstract bool ConditionValidated();
}
