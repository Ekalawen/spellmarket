using PlasticPipe.PlasticProtocol.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class ComboConditionalEffect : ConditionalEffect {

    public override bool ConditionValidated() {
        List<Card> otherCards = GetSelf().hand.GetAllCards().FindAll(c => c != card);
        return otherCards.Any(c => c.IsCombo());
    }

    public override bool IsCombo() {
        return true;
    }
}
