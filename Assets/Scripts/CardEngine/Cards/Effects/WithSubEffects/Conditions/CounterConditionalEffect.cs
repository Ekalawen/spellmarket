using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class CounterConditionalEffect : ConditionalEffect {

    public Target target;
    public CounterType counter;
    public Comparator comparator;
    public int treshold;

    public override bool ConditionValidated() {
        List<Player> players = GetTargets(target);
        return players.All(ConditionValidatedForPlayer);
    }

    protected bool ConditionValidatedForPlayer(Player player) {
        Counter consideredCounter = counter == CounterType.HEALTH ? player.health : player.power;
        int value = consideredCounter.GetValue();
        return ApplyComparator(comparator, value, treshold);
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        CounterConditionalEffect otherEffect = (CounterConditionalEffect)other;
        target = otherEffect.target;
        counter = otherEffect.counter;
        comparator = otherEffect.comparator;
        treshold = otherEffect.treshold;
    }
}
