using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class OwnEnchantmentConditionalEffect : ConditionalEffect {

    public Target target;
    public bool shouldOwn = true;

    public override bool ConditionValidated() {
        List<Player> players = GetTargets(target);
        return players.All(ConditionValidatedForPlayer);
    }

    protected bool ConditionValidatedForPlayer(Player player) {
        return player.hand.GetAllCards().Any(c => c.IsEnchantment()) == shouldOwn;
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        OwnEnchantmentConditionalEffect otherEffect = (OwnEnchantmentConditionalEffect)other;
        target = otherEffect.target;
        shouldOwn = otherEffect.shouldOwn;
    }
}
