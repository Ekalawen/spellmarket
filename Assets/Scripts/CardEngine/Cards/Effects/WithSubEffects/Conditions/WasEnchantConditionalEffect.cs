using PlasticPipe.PlasticProtocol.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class WasEnchantConditionalEffect : ConditionalEffect {

    public int cardIndex = 0;
    public bool shouldBe = true;

    public override bool ConditionValidated() {
        Card card = Select().GetSelectedCard(index: cardIndex);
        return shouldBe == card.IsEnchantment();
    }
}
