using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class CounterComparedConditionalEffect : ConditionalEffect {

    public Target target;
    public CounterType counter;
    public Comparator comparator;

    public override bool ConditionValidated() {
        List<Player> players = GetTargets(target);
        return players.All(ConditionValidatedForPlayer);
    }

    protected bool ConditionValidatedForPlayer(Player player) {
        Player opponent = game.playerManager.GetOtherPlayer(player);
        Counter consideredCounter = counter == CounterType.HEALTH ? player.health : player.power;
        Counter oppositeCounter = counter == CounterType.HEALTH ? opponent.health : opponent.power;
        return ApplyComparator(comparator, consideredCounter.GetValue(), oppositeCounter.GetValue());
    }


    public override void Copy(Effect other) {
        base.Copy(other);
        CounterComparedConditionalEffect otherEffect = (CounterComparedConditionalEffect)other;
        target = otherEffect.target;
        counter = otherEffect.counter;
        comparator = otherEffect.comparator;
    }
}
