using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class OwnCardColorConditionalEffect : ConditionalEffect {

    public Target target;
    public CardColor color;
    public bool shouldOwn = true;
    public bool otherCard = false;

    public override bool ConditionValidated() {
        List<Player> players = GetTargets(target);
        return players.All(ConditionValidatedForPlayer);
    }

    protected bool ConditionValidatedForPlayer(Player player) {
        return player.hand.GetAllCards().Any(c => IsGoodCard(c)) == shouldOwn;
    }

    protected bool IsGoodCard(Card card) {
        return (!otherCard || card != GetCard())
            && card.color == color;
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        OwnCardColorConditionalEffect otherEffect = (OwnCardColorConditionalEffect)other;
        target = otherEffect.target;
        color = otherEffect.color;
        shouldOwn = otherEffect.shouldOwn;
    }
}
