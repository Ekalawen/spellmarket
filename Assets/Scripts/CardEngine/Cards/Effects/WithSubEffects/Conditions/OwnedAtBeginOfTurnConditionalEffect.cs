using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class OwnedAtBeginOfTurnConditionalEffect : ConditionalEffect {


    protected Player owningPlayer = null;

    public override bool ConditionValidated() {
        return owningPlayer == GetSelf();
    }

    public override void OnControlledBy(Player player, Zone previousZone) {
        base.OnControlledBy(player, previousZone);
        player.onBeginTurn.AddListener(OnBeginTurn);
    }

    protected void OnBeginTurn() {
        owningPlayer = GetSelf();
    }

    public override void OnStopControlledBy(Player player, Zone newZone) {
        base.OnStopControlledBy(player, newZone);
        owningPlayer = null;
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        OwnedAtBeginOfTurnConditionalEffect otherEffect = (OwnedAtBeginOfTurnConditionalEffect)other;
        owningPlayer = otherEffect.owningPlayer;
    }
}
