using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class ElseConditionalEffect : EffectWithSubEffect {

    public Effect elseSubEffect;

    protected ConditionalEffect conditionalSubEffect;

    public override void Initialize(Card card) {
        base.Initialize(card);
        Assert.IsTrue(subEffect.GetType().IsSubclassOf(typeof(ConditionalEffect)));
        conditionalSubEffect = (ConditionalEffect)subEffect;
        elseSubEffect.Initialize(card);
    }

    public override void Trigger() {
        if(conditionalSubEffect.ConditionValidated()) {
            conditionalSubEffect.Trigger();
        } else {
            elseSubEffect.Trigger();
        }
    }
}
