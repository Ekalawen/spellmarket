using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class RemoveOnPlayEffect : EffectWithSubEffect {


    public override void Initialize(Card card) {
        base.Initialize(card);
        card.onPlay.AddListener(RemoveEffect);
    }

    protected void RemoveEffect() {
        card.onPlay.RemoveListener(RemoveEffect);
        card.RemoveEffect(this);
    }
}
