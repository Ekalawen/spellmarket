using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class OnAllBeginTurnEffect : EffectWithSubEffect {

    public bool useBeforeBeginTurn = true;

    public override void Initialize(Card card) {
        base.Initialize(card);
        if (useBeforeBeginTurn) {
            game.phaseManager.onBeforeBeginTurn.AddListener(subEffect.Trigger);
        } else {
            game.phaseManager.onBeginTurn.AddListener(subEffect.Trigger);
        }
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        OnAllBeginTurnEffect otherEffect = (OnAllBeginTurnEffect)other;
        useBeforeBeginTurn = otherEffect.useBeforeBeginTurn;
    }
}
