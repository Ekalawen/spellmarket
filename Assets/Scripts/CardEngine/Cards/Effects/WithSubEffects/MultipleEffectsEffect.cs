using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class MultipleEffectsEffect : Effect {

    public List<Effect> subEffects;

    public override void Initialize(Card card) {
        base.Initialize(card);
        if(subEffects.Count < 2) {
            Debug.LogError($"{this} dans la carte {card} n'a pas au moins 2 subEffects !");
        }
        subEffects.ForEach(subEffect => subEffect.Initialize(card));
    }

    public override void Copy(Effect other) {
        MultipleEffectsEffect multipleEffects = (MultipleEffectsEffect)other;
        subEffects = multipleEffects.subEffects.Select(s => s).ToList();
    }

    public override void Trigger() {
        subEffects.ForEach(subEffect => subEffect.Trigger());
    }

    public override bool IsCombo() {
        return subEffects.Any(s => s.IsCombo());
    }

    public override bool CardMustBeSelected() {
        return subEffects.Any(s => s.CardMustBeSelected());
    }

    public override bool CardMustBePlayed() {
        return subEffects.Any(s => s.CardMustBePlayed());
    }
}
