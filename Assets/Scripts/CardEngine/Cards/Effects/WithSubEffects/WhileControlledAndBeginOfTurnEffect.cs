using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class WhileControlledAndBeginOfTurnEffect : EffectWithSubEffect {

    public override void OnControlledBy(Player player, Zone previousZone) {
        base.OnControlledBy(player, previousZone);
        subEffect.Trigger();
        player.onBeginTurn.AddListener(subEffect.Trigger);
    }

    public override void OnStopControlledBy(Player player, Zone newZone) {
        base.OnStopControlledBy(player, newZone);
        subEffect.Stop();
        player.onBeginTurn.RemoveListener(subEffect.Trigger);
    }
}
