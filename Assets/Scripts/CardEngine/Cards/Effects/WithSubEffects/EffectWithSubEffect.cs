using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class EffectWithSubEffect : Effect {

    public Effect subEffect;

    public override void Initialize(Card card) {
        base.Initialize(card);
        if(subEffect == null) {
            Debug.LogError($"{this} dans la carte {card} a son subEffet null !");
        }
        subEffect.Initialize(card);
    }

    public override void Copy(Effect other) {
        EffectWithSubEffect otherEffectWithSubEffect = (EffectWithSubEffect)other;
        subEffect = otherEffectWithSubEffect.subEffect;
    }

    public override void Trigger() {
        subEffect.Trigger();
    }

    public override bool IsCombo() {
        return subEffect.IsCombo();
    }

    public override bool CardMustBeSelected() {
        return subEffect.CardMustBeSelected();
    }

    public override bool CardMustBePlayed() {
        return subEffect.CardMustBePlayed();
    }
}
