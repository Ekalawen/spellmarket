using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class Effect : GameMonoBehaviour
{
    public enum Target {
        SELF,
        OPPONENT,
        BOTH,
    }

    public enum CounterType {
        HEALTH,
        POWER,
    }

    public enum CounterDirection {
        GONE_UP,
        GONE_DOWN,
    }

    public enum Comparator {
        GREATER_OR_EQUAL,
        LESSER_OR_EQUAL,
        STRICTLY_GREATER,
        STRICTLY_LESSER,
        EQUAL,
    }

    public enum ZoneType {
        SELF_HAND,
        OPPONENT_HAND,
        RIVER,
        BOTH_HANDS,
        TEMPORARY,
    }


    protected Card card;
    protected bool isAdded = false;
    protected SelectedCardsComponent selectedCardsComponent;

    public virtual void Initialize(Card card) {
        this.card = card;
        selectedCardsComponent = new SelectedCardsComponent();
        card.onControlledBy.AddListener(OnControlledBy);
        card.onStopControlledBy.AddListener(OnStopControlledBy);
        card.onVisible.AddListener(OnVisible);
        card.onStopVisible.AddListener(OnStopVisible);
    }

    public abstract void Copy(Effect other);

    public abstract void Trigger();

    public virtual void Stop() {
    }

    public Player GetSelf() {
        if (card.CurrentZone() == game.player1.hand) {
            return game.player1;
        }
        if (card.CurrentZone() == game.player2.hand) {
            return game.player2;
        }
        /// If we let this, the Replay last card will play for the opponent ^^
        //if(card.PreviousZone() == game.player1.hand) {
        //    return game.player1;
        //}
        //if (card.PreviousZone() == game.player2.hand) {
        //    return game.player2;
        //}
        return Game.Instance.playerManager.CurrentPlayer();
    }

    public Player GetOpponent() {
        return game.playerManager.GetOtherPlayer(GetSelf());
    }

    public virtual void OnControlledBy(Player player, Zone previousZone) {
    }

    public virtual void OnStopControlledBy(Player player, Zone newZone) {
    }

    public virtual void OnVisible() {
    }

    public virtual void OnStopVisible() {
    }

    public List<Player> GetTargets(Target target) {
        switch(target) {
            case Target.SELF:
                return new List<Player> { GetSelf() };
            case Target.OPPONENT:
                return new List<Player> { GetOpponent() };
            case Target.BOTH:
                return new List<Player> { GetSelf(), GetOpponent() };
            default:
                throw new Exception("Unknown target");
        }
    }

    public Player GetSingleTarget(Target target) {
        switch(target) {
            case Target.SELF:
                return GetSelf();
            case Target.OPPONENT:
                return GetOpponent();
            case Target.BOTH:
                throw new Exception("Target is both");
            default:
                throw new Exception("Unknown target");
        }
    }

    public virtual bool IsCombo() {
        return false;
    }

    public virtual bool CardMustBeSelected() {
        return false;
    }

    public virtual bool CardMustBePlayed() {
        return false;
    }

    public bool ApplyComparator(Comparator comparator, int value1, int value2) {
        switch(comparator)
        {
            case Comparator.GREATER_OR_EQUAL:
                return value1 >= value2;
            case Comparator.LESSER_OR_EQUAL:
                return value1 <= value2;
            case Comparator.STRICTLY_GREATER:
                return value1 > value2;
            case Comparator.STRICTLY_LESSER:
                return value1 < value2;
            case Comparator.EQUAL:
                return value1 == value2;
            default:
                throw new Exception("Unknown comparator");
        }
    }

    public Card GetCard() {
        return card;
    }

    public virtual bool CardIsInstaPlayed() {
        return true;
    }

    public bool IsAdded() {
        return isAdded;
    }

    public void SetAdded() {
        isAdded = true;
    }

    protected List<Zone> GetZones(ZoneType zoneType) {
        switch (zoneType) {
            case ZoneType.SELF_HAND:
                return new List<Zone> { GetSelf().hand };
            case ZoneType.OPPONENT_HAND:
                return new List<Zone> { GetOpponent().hand };
            case ZoneType.RIVER:
                return new List<Zone> { game.river };
            case ZoneType.BOTH_HANDS:
                return new List<Zone> { GetSelf().hand, GetOpponent().hand };
            default:
                throw new Exception("Unknown zone type");
        }
    }

    public SelectedCardsComponent Select() {
        return selectedCardsComponent;
    }
}
