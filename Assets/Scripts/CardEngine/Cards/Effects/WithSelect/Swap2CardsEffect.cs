using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class Swap2CardsEffect : Effect {

    public override void Trigger() {
        Card card1 = Select().GetSelectedCard(index: 0);
        Card card2 = Select().GetSelectedCard(index: 1);
        SwapCards(card1, card2);
    }

    public void SwapCards(Card card1, Card card2) {
        Zone card1Zone = card1.CurrentZone();
        Zone card2Zone = card2.CurrentZone();
        card1.MoveTo(card2Zone);
        card2.MoveTo(card1Zone);
    }

    public override void Copy(Effect other) {
    }
}
