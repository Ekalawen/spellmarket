using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AddEffectToCardEffect : Effect {

    public bool addFirst = true;
    public Effect effectToAdd;

    public override void Trigger() {
        Card card = Select().GetSelectedCard();
        card.AddEffect(effectToAdd, addFirst: addFirst);
    }

    public override void Copy(Effect other) {
        AddEffectToCardEffect otherEffect = (AddEffectToCardEffect)other;
        effectToAdd = otherEffect.effectToAdd;
    }
}
