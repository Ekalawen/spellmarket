using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ReplaceCardEffect : Effect {

    public enum ReplaceCardEffectType {
        Select,
        Draw,
    }

    public ReplaceCardEffectType type = ReplaceCardEffectType.Select;

    public override void Trigger() {
        Card discardedCard = Select().GetSelectedCard(index: 0);
        Zone discardedCardZone = discardedCard.CurrentZone();
        discardedCard.MoveTo(game.discard, isPlayMove: false);
        Card replacementCard = type == ReplaceCardEffectType.Select ? Select().GetSelectedCard(1) : game.deck.Draw();
        replacementCard.MoveTo(discardedCardZone, isPlayMove: true);
    }

    public override void Copy(Effect other) {
    }
}
