using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlaySelectedCardEffect : Effect {

    public override void Trigger() {
        PlayCard(cardIndex: 0, player: GetSelf());
    }

    protected void PlayCard(int cardIndex, Player player) {
        Card selectedCard = Select().GetSelectedCard(index: cardIndex);
        Zone selectedCardZone = selectedCard.CurrentZone();
        if (selectedCardZone != player.hand) {
            selectedCard.MoveTo(player.hand, isPlayMove: false);
        }
        selectedCard.Play();
    }

    public override void Copy(Effect other) {
    }
}
