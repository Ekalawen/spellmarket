using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class SelectedCardsComponent {

    protected List<Card> selectedCard;

    public SelectedCardsComponent() {
        selectedCard = new List<Card>();
    }

    public void AddSelectedCard(Card card) {
        selectedCard.Add(card);
    }

    public static void PropagateAddSelectedCard(Effect effect, Card addedCard) {
        effect.Select().AddSelectedCard(addedCard);
        if (effect is EffectWithSubEffect) {
            PropagateAddSelectedCard((effect as EffectWithSubEffect).subEffect, addedCard);
        }
        if(effect is MultipleEffectsEffect) {
            (effect as MultipleEffectsEffect).subEffects.ForEach(subEffect => PropagateAddSelectedCard(subEffect, addedCard));
        }
    }

    public Card GetSelectedCard(int index = 0) {
        return selectedCard[index];
    }

    public List<Card> GetSelectedCards() {
        return selectedCard;
    }

    public void ClearCards() {
        selectedCard.Clear();
    }

    public static void PropagateClearCards(Effect effect) {
        effect.Select().ClearCards();
        if (effect is EffectWithSubEffect) {
            PropagateClearCards((effect as EffectWithSubEffect).subEffect);
        }
        if(effect is MultipleEffectsEffect) {
            (effect as MultipleEffectsEffect).subEffects.ForEach(subEffect => PropagateClearCards(subEffect));
        }
    }

    public void CopyFrom(SelectedCardsComponent other) {
        selectedCard = other.selectedCard.Select(c => c).ToList();
    }
}
