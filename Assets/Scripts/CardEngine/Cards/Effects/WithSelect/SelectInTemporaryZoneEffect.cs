using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class SelectInTemporaryZoneEffect : SelectCardEffect {

    public int nbCardsToDraw = 3;

    protected List<Card> cardsToDiscard = null;

    public override void Initialize(Card card) {
        base.Initialize(card);
        Assert.IsTrue(zones.All(z => z == ZoneType.TEMPORARY));
        onFinishSelection.AddListener(DiscardTemporaryZone);
    }

    public override void Trigger() {
        FillTemporaryZone();
        base.Trigger();
    }

    protected void DiscardTemporaryZone() {
        if(cardsToDiscard == null) {
            return;
        }
        foreach(Card card in cardsToDiscard) {
            if(Select().GetSelectedCards().Contains(card)) {
                continue;
            }
            card.MoveTo(game.discard, isPlayMove: false);
        }
    }

    private void FillTemporaryZone() {
        game.deck.DrawMultiple(nbCardsToDraw).ForEach(c => c.MoveTo(game.temporaryZone, isPlayMove: false));
        cardsToDiscard = game.temporaryZone.GetAllCards();
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        SelectInTemporaryZoneEffect otherEffect = (SelectInTemporaryZoneEffect)other;
        nbCardsToDraw = otherEffect.nbCardsToDraw;
    }
}
