using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using static ReplaceCardEffect;

public class ReplaceCardWithSubEffect : EffectWithSubEffect {

    public ReplaceCardEffectType type = ReplaceCardEffectType.Select;

    public override void Trigger()
    {
        Card discardedCard = Select().GetSelectedCard(index: 0);
        Zone discardedCardZone = discardedCard.CurrentZone();
        discardedCard.MoveTo(game.discard, isPlayMove: false);
        Card replacementCard = type == ReplaceCardEffectType.Select ? Select().GetSelectedCard(1) : game.deck.Draw();
        replacementCard.MoveTo(discardedCardZone, isPlayMove: true);
        PropagateCards(subEffect, discardedCard, replacementCard);
        subEffect.Trigger();
    }

    protected void PropagateCards(Effect effect, Card discardedCard, Card replacementCard) {
        SelectedCardsComponent.PropagateClearCards(effect);
        SelectedCardsComponent.PropagateAddSelectedCard(effect, discardedCard);
        SelectedCardsComponent.PropagateAddSelectedCard(effect, replacementCard);
    }

    public override void Copy(Effect other) {
    }
}
