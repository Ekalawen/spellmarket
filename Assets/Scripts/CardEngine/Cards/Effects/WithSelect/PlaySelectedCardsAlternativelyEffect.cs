using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlaySelectedCardsAlternativelyEffect : PlaySelectedCardEffect {

    public override void Trigger() {
        PlayCard(cardIndex: 0, player: GetSelf());
        PlayCard(cardIndex: 1, player: GetOpponent());
        PlayCard(cardIndex: 2, player: GetSelf());
    }

    public override void Copy(Effect other) {
    }
}
