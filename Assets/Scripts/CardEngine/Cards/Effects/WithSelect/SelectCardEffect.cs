using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class SelectCardEffect : EffectWithSubEffect {

    public List<ZoneType> zones;
    public bool canAutoSelect = false;
    public CardAction cardAction;

    protected int nbCardsToSelect;
    protected int currentNbCardsSelected;
    [HideInInspector]
    public UnityEvent onFinishSelection = new UnityEvent();

    public override void Initialize(Card card) {
        base.Initialize(card);
        Assert.IsTrue(zones.Count > 0);
        nbCardsToSelect = zones.Count;
    }

    public override void Trigger() {
        InitNbOfCards();
        DisablePreviousPhaseAndCardAction();
        StartCardAction();
    }

    private void InitNbOfCards()
    {
        currentNbCardsSelected = 0;
        SelectedCardsComponent.PropagateClearCards(this);
        //Select().ClearCards();
        //subEffect.Select().ClearCards();
    }

    protected void StartCardAction() {
        cardAction.canSelectAnyCard = false;
        cardAction.sourceCardZone = GetZone();
        cardAction.Initialize();
        if(!canAutoSelect) {
            cardAction.AddUnselectableCard(card);
        }
        cardAction.AddUnselectableCards(Select().GetSelectedCards());
        cardAction.Enable();
        cardAction.onPerform.AddListener(OnCardActionPerform);
    }

    protected void DisablePreviousPhaseAndCardAction() {
        Phase playPhase = ((TurnSequencer)game.turnSequencer.CurrentPhase()).phases[1];
        playPhase.ender.Stop();
        CardAction currentCardAction = game.actionManager.GetCurrentCardAction();
        if (currentCardAction != null) {
            currentCardAction.Disable();
        }
    }

    protected void ReactivateAndEndPreviousPhase() {
        if(game.actionManager.HasCardAction()) {
            return;
        }
        Phase playPhase = ((TurnSequencer)game.turnSequencer.CurrentPhase()).phases[1];
        playPhase.ender.Initialize(playPhase);
        playPhase.ender.End();
        //playPhase.ender.EndWhenNoCardAction();
    }

    protected void OnCardActionPerform() {
        cardAction.Disable();
        cardAction.onPerform.RemoveListener(OnCardActionPerform);
        SelectedCardsComponent.PropagateAddSelectedCard(this, cardAction.GetSelectedCard());
        currentNbCardsSelected++;

        if (currentNbCardsSelected == nbCardsToSelect) {
            subEffect.Trigger();
            card.ContinuePlay();
            ReactivateAndEndPreviousPhase();
            onFinishSelection.Invoke();
        } else {
            StartCardAction();
        }
    }

    private Zone GetZone() {
        switch (zones[currentNbCardsSelected]) {
            case ZoneType.SELF_HAND:
                return GetSelf().hand;
            case ZoneType.OPPONENT_HAND:
                return GetOpponent().hand;
            case ZoneType.RIVER:
                return game.river;
            case ZoneType.TEMPORARY:
                return game.temporaryZone;
            default:
                throw new Exception("Unknown zone type");
        }
    }

    public override void Copy(Effect other) {
        base.Copy(other);
        SelectCardEffect otherSelectCardEffect = (SelectCardEffect)other;
        zones = otherSelectCardEffect.zones;
        canAutoSelect = otherSelectCardEffect.canAutoSelect;
        cardAction = otherSelectCardEffect.cardAction;
        nbCardsToSelect = otherSelectCardEffect.nbCardsToSelect;
        currentNbCardsSelected = otherSelectCardEffect.currentNbCardsSelected;
    }
}
