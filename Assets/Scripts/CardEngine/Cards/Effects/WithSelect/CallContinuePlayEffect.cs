using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class CallContinuePlayEffect : Effect {

    public override void Trigger() {
        GetCard().ContinuePlay();
    }

    public override void Copy(Effect other) {
    }
}
