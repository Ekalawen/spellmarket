using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using MyBox;
using System.Linq;

public class Card : GameMonoBehaviour {

    protected static int nextId = 0;

    [ReadOnly]
    public int id;
    [SerializeField, ReadOnly]
    protected Zone currentZone = null;
    protected Zone previousZone = null;
    public CardType type;
    public CardColor color;
    public List<Effect> effects;

    protected bool isSelectable = false;
    protected bool hasBeenSelectedThisTurn = false;

    [HideInInspector]
    public UnityEvent onInitialize = new UnityEvent();
    [HideInInspector]
    public UnityEvent onPlay = new UnityEvent();
    [HideInInspector]
    public UnityEvent onPreparePlay = new UnityEvent();
    [HideInInspector]
    public UnityEvent onNotPlayTriggered = new UnityEvent();
    [HideInInspector]
    public UnityEvent<Zone, Zone, bool> onMove = new UnityEvent<Zone, Zone, bool>(); // The first argument is the previous zone, the second is the new zone, the third if true if its a "Play Card" move
    [HideInInspector]
    public UnityEvent<Player, Zone> onControlledBy = new UnityEvent<Player, Zone>(); // When the card is put in front of a player, the first argument is the player, the second is the previous zone
    [HideInInspector]
    public UnityEvent<Player, Zone> onStopControlledBy = new UnityEvent<Player, Zone>(); // The first argument is the player, the second is the new zone
    [HideInInspector]
    public UnityEvent onVisible = new UnityEvent();
    [HideInInspector]
    public UnityEvent onStopVisible = new UnityEvent();
    [HideInInspector]
    public UnityEvent onStartSelectable = new UnityEvent();
    [HideInInspector]
    public UnityEvent onStopSelectable = new UnityEvent();
    [HideInInspector]
    public UnityEvent onChangeEffects = new UnityEvent();

    public virtual void Initialize() {
        InitId();
        InitEffects();
        SetSelectable(false);
        game.phaseManager.onEndTurn.AddListener(ResetHasBeenSelectedThisTurn);
        game.endGameChecker.onGameBegin.AddListener(CheckBeginOfGameControlledBy);
        game.endGameChecker.onGameBegin.AddListener(CheckBeginOfGameVisible);
    }

    protected void InitEffects() {
        foreach(Effect effect in effects) {
            if(effect == null) {
                Debug.LogError($"{this} a un effet null !");
            }
            effect.Initialize(this);
        }
    }

    protected void InitId() {
        id = nextId;
        nextId++;
    }

    public override bool Equals(object other) {
        if (other == null) return false;
        if (other.GetType() != this.GetType()) return false;
        Card otherCard = (Card)other;
        if (otherCard == this) return true;
        return id == otherCard.id;
    }

    public override int GetHashCode() {
        return id;
    }

    public Zone CurrentZone() {
        return currentZone;
    }

    public Zone PreviousZone() {
        return previousZone;
    }

    public virtual bool CanMoveTo(Zone zone) {
        return (CurrentZone() == null || CurrentZone().CanRemove(this))
            && zone.CanAdd(this);
    }

    public void MoveTo(Zone newZone, bool isPlayMove = false) {
        Assert.IsTrue(CanMoveTo(newZone));
        Zone previousZone = CurrentZone();
        if (previousZone != null) {
            previousZone.Remove(this);
        }
        newZone.Add(this);
        onMove.Invoke(previousZone, newZone, isPlayMove);
    }

    public void Play() {
        if (IsInstaPlayed()) {
            PlayNow();
        } else {
            PreparePlay();
        }
    }

    protected void PlayNow() {
        Player owner = GetOwner();
        onPlay.Invoke();
        owner.onPlay.Invoke(owner, this);
        game.playerManager.onPlay.Invoke(owner, this);
        MoveTo(game.discard, isPlayMove: true);
    }

    protected void PreparePlay() {
        onPreparePlay.Invoke();
        // The card effect will have to call manually the ContinuePlay method !
    }

    public void ContinuePlay() {
        PlayNow();
    }

    public Player GetOwner() {
        if(CurrentZone() == game.player1.hand) {
            return game.player1;
        }
        if(CurrentZone() == game.player2.hand) {
            return game.player2;
        }
        Debug.LogWarning($"The card {this} is not played from hand !");
        return game.playerManager.CurrentPlayer();
    }

    public void SetCurrentZone(Zone zone)
    {
        if (zone == currentZone) return;
        previousZone = currentZone;
        currentZone = zone;
        CallOnControlledByEvents(zone, previousZone);
        CallOnVisibleEvents(zone, previousZone);
    }

    protected void CallOnVisibleEvents(Zone newZone, Zone previousZone) {
        if(!game.endGameChecker.IsGameStarted()) {
            return;
        }
        if (IsVisibleZone(newZone) && !IsVisibleZone(previousZone)) {
            onVisible.Invoke();
        } else if (!IsVisibleZone(newZone) && IsVisibleZone(previousZone)) {
            onStopVisible.Invoke();
        }
    }

    protected void CheckBeginOfGameVisible() {
        if (IsVisibleZone(CurrentZone())) {
            onVisible.Invoke();
        }
    }

    protected void CheckBeginOfGameControlledBy() {
        if (CurrentZone() == game.player1.hand) {
            onControlledBy.Invoke(game.player1, game.deck);
        } else if (CurrentZone() == game.player2.hand) {
            onControlledBy.Invoke(game.player2, game.deck);
        }
    }

    protected bool IsVisibleZone(Zone zone) {
        return zone == game.player1.hand || zone == game.player2.hand || zone == game.river;
    }

    protected void CallOnControlledByEvents(Zone newZone, Zone previousZone) {
        if(!game.endGameChecker.IsGameStarted()) {
            return;
        }
        if (previousZone == game.player1.hand) {
            onStopControlledBy.Invoke(game.player1, newZone);
        } else if (previousZone == game.player2.hand) {
            onStopControlledBy.Invoke(game.player2, newZone);
        }
        if (newZone == game.player1.hand) {
            onControlledBy.Invoke(game.player1, previousZone);
        } else if (newZone == game.player2.hand) {
            onControlledBy.Invoke(game.player2, previousZone);
        }
    }

    public void SetSelectable(bool selectable) {
        if(isSelectable == selectable) {
            return;
        }
        isSelectable = selectable;
        if (selectable) {
            onStartSelectable.Invoke();
        } else {
            onStopSelectable.Invoke();
        }
    }

    public bool IsSelectable() {
        return isSelectable;
    }

    public bool IsEnchantment() {
        return type == CardType.ENCHANTMENT;
    }

    public bool IsInstaPlayed() {
        return effects.All(e => e.CardIsInstaPlayed());
    }

    public bool IsCombo() {
        return effects.Any(e => e.IsCombo());
    }

    public bool MustBeSelected() {
        return effects.Any(e => e.CardMustBeSelected());
    }

    public bool MustBePlayed() {
        return effects.Any(e => e.CardMustBePlayed());
    }

    public void SetHasBeenSelectedThisTurn() {
        hasBeenSelectedThisTurn = true;
    }

    protected void ResetHasBeenSelectedThisTurn() {
        hasBeenSelectedThisTurn = false;
    }

    public bool HasBeenSelectedThisTurn() {
        return hasBeenSelectedThisTurn;
    }

    public void AddEffect(Effect effectToCopy, bool addFirst = false) {
        Effect newEffect = gameObject.AddComponent(effectToCopy.GetType()) as Effect;
        if (addFirst) {
            effects.Insert(0, newEffect);
        } else {
            effects.Add(newEffect);
        }
        newEffect.SetAdded();
        newEffect.Copy(effectToCopy);
        newEffect.Initialize(this);
        onChangeEffects.Invoke();
    }

    public void RemoveEffect(Effect effectToRemove) {
        if(!effects.Contains(effectToRemove)) {
            return;
        }
        effects.Remove(effectToRemove);
        Destroy(effectToRemove);
        onChangeEffects.Invoke();
    }
}

