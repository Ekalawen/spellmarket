using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ActionManager : GameMonoBehaviour {

    [SerializeField, ReadOnly]
    protected List<Action> currentActions;
    [SerializeField, ReadOnly]
    protected List<CardAction> currentCardActions;

    [HideInInspector]
    public PriorityEvent<Action> onActionEnd = new PriorityEvent<Action>();
    [HideInInspector]
    public PriorityEvent<CardAction> onCardActionEnd = new PriorityEvent<CardAction>();

    public void Initialize() {
        currentActions = new List<Action>();
        currentCardActions = new List<CardAction>();
    }

    public void RegisterCurrentAction(Action action) {
        if(currentActions.Contains(action)) {
            return;
        }
        currentActions.Add(action);
        if(action is CardAction) {
            currentCardActions.Add(action as CardAction);
        }
    }

    public void UnregisterCurrentAction(Action action) {
        bool removed = currentActions.Remove(action);
        if(action is CardAction) {
            bool cardRemoved = currentCardActions.Remove(action as CardAction);
            if(cardRemoved) {
                onCardActionEnd.Invoke(action as CardAction);
            }
        }
        if(removed) {
            onActionEnd.Invoke(action);
        }
    }

    public CardAction GetCurrentCardAction() {
        if (currentCardActions.Count == 0) {
            return null;
        }
        if (currentCardActions.Count > 1) {
            string actions = "";
            foreach(CardAction action in currentCardActions) {
                actions += action + " ";
            }
            Debug.LogError($"More than one card action is currently active !! Actions = {actions}");
        }
        return currentCardActions.First();
    }

    public bool HasCardAction() {
        return currentCardActions.Count > 0;
    }
}
