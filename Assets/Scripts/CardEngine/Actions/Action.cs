using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Action : GameMonoBehaviour {

    [HideInInspector]
    public UnityEvent onPerform = new UnityEvent();
    [HideInInspector]
    public UnityEvent onEnable = new UnityEvent();
    [HideInInspector]
    public UnityEvent onDisable = new UnityEvent();

    protected ActionManager actionManager;

    public virtual void Initialize() {
        actionManager = game.actionManager;
    }

    public virtual bool CanPerform() {
        return true;
    }

    public void Perform() {
        Assert.IsTrue(CanPerform());
        PerformSpecific();
        onPerform.Invoke();
    }

    protected virtual void PerformSpecific() {
    }

    public virtual void Enable() {
        actionManager.RegisterCurrentAction(this);
        onEnable.Invoke();
    }

    public virtual void Disable() {
        actionManager.UnregisterCurrentAction(this);
        onDisable.Invoke();
    }
}
