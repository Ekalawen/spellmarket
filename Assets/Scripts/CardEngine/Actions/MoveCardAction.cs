using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MoveCardAction : CardAction {

    public Zone targetZone;

    public override bool CanPerform() {
        return base.CanPerform()
            && targetZone != null
            && selectedCard.CanMoveTo(targetZone);
    }

    protected override void PerformSpecific() {
        Assert.IsTrue(CanPerform());
        selectedCard.MoveTo(targetZone);
    }
}
