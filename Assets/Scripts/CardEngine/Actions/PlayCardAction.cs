using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayCardAction : CardAction {

    protected Zone discard => Game.Instance.discard;

    public override bool CanPerform() {
        return base.CanPerform()
            && discard.CanAdd(selectedCard)
            && selectedCard.CurrentZone().CanRemove(selectedCard);
    }

    protected override void PerformSpecific()
    {
        Assert.IsTrue(CanPerform());
        bool replayCard = game.playerManager.CurrentPlayer().AreCardsPlayedTwice();
        selectedCard.Play();
        TryReplayCard(replayCard);
    }

    protected void TryReplayCard(bool replayCard) {
        if(!replayCard) {
            return;
        }
        if (selectedCard.IsInstaPlayed()) {
            selectedCard.Play();
        } else {
            // C'est le meilleur compromis entre efficacité de développement et réalité :)
            // On skip juste les onPrepared effects.
            selectedCard.onPlay.Invoke();
        }
    }

    public override List<Card> FilterSelectableCards(List<Card> cards) {
        if (cards.Any(c => c.MustBePlayed())) {
            cards = cards.FindAll(c => c.MustBePlayed());
        }
        return cards;
    }
}
