using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class SelectAndMoveCardAction : MoveCardAction {

    protected override void PerformSpecific() {
        base.PerformSpecific();
        selectedCard.SetHasBeenSelectedThisTurn();
    }

    public override List<Card> FilterSelectableCards(List<Card> cards) {
        if (cards.Any(c => c.MustBeSelected())) {
            cards = cards.FindAll(c => c.MustBeSelected());
        }
        return cards;
    }
}
