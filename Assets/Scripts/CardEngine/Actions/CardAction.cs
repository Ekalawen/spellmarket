using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class CardAction : Action {

    public bool canSelectAnyCard = true;
    [ConditionalField("canSelectAnyCard", true)]
    public Zone sourceCardZone = null;

    protected List<Card> selectableCards;
    protected Card selectedCard;
    protected List<Card> unselectableCards;

    public override void Initialize() {
        base.Initialize();
        selectedCard = null;
        unselectableCards = new List<Card>();
    }

    public virtual bool CanSelectCard(Card card) {
        return canSelectAnyCard || GetAllSelectableCards().Contains(card);
    }

    public void SelectCard(Card card) {
        Assert.IsTrue(CanSelectCard(card));
        selectedCard = card;
    }

    public override bool CanPerform() {
        return base.CanPerform() && selectedCard != null;
    }

    public List<Card> GetAllSelectableCards() {
        List<Card> cardsCandidates = canSelectAnyCard ? game.pool.InstantiatedCards() : sourceCardZone.GetAllCards();
        cardsCandidates = cardsCandidates.Except(unselectableCards).ToList();
        return FilterSelectableCards(cardsCandidates);
    }

    public virtual List<Card> FilterSelectableCards(List<Card> cards) {
        return cards;
    }

    public override void Enable() {
        base.Enable();
        selectableCards = new List<Card>();
        foreach (Card card in GetAllSelectableCards()) {
            card.SetSelectable(true);
            selectableCards.Add(card);
        }
    }

    public override void Disable() {
        base.Disable();
        foreach (Card card in selectableCards) {
            card.SetSelectable(false);
        }
    }

    public Card GetSelectedCard() {
        return selectedCard;
    }

    public void AddUnselectableCard(Card card) {
        unselectableCards.Add(card);
    }

    public void AddUnselectableCards(List<Card> cards) {
        unselectableCards.AddRange(cards);
    }
}
