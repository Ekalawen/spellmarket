using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class DeckZone : Zone {

    public override void Initialize() {
        base.Initialize();
    }

    public void Shuffle() {
        MathTools.Shuffle(cards);
    }

    public bool CanDraw() {
        return cards.Count > 0;
    }

    public bool CanDrawMultiple(int quantity) {
        return cards.Count >= quantity;
    }

    public Card Draw() {
        Assert.IsTrue(CanDraw());
        Card card = cards.First();
        Remove(card);
        card.SetCurrentZone(null);
        return card;
    }

    public List<Card> DrawMultiple(int quantity) {
        //Assert.IsTrue(CanDrawMultiple(quantity)); // ==> Already asserted in Draw, and better here because we can use the deckFiller :)
        List<Card> cards = new List<Card>();
        for (int i = 0; i < quantity; i++) {
            cards.Add(Draw());
        }
        return cards;
    }
}
