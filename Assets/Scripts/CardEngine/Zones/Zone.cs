using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Zone : MonoBehaviour {

    public bool insertFirst = false;

    [HideInInspector]
    public UnityEvent<Card> onAddCard = new UnityEvent<Card>();
    [HideInInspector]
    public UnityEvent<Card> onRemoveCard = new UnityEvent<Card>();

    protected List<Card> cards;

    public int Count => cards.Count;

    public object Select { get; internal set; }

    public virtual void Initialize() {
        cards = new List<Card>();
    }

    public virtual bool CanAdd(Card card) {
        return true;
    }

    public virtual void Add(Card card) {
        Assert.IsTrue(CanAdd(card));
        if (insertFirst) {
            cards.Insert(0, card);
        } else {
            cards.Add(card);
        }
        card.SetCurrentZone(this);
        onAddCard.Invoke(card);
    }

    public void Add(List<Card> cardList) {
        foreach (Card card in cardList) {
            Assert.IsTrue(CanAdd(card));
            Add(card);
        }
    }

    public virtual bool CanRemove(Card card) {
        return cards.Contains(card);
    }

    public virtual Card Remove(Card card) {
        Assert.IsTrue(CanRemove(card));
        cards.Remove(card);
        onRemoveCard.Invoke(card);
        return card;
    }

    public List<Card> Remove(List<Card> cards) {
        foreach (Card card in cards) {
            Assert.IsTrue(CanRemove(card));
            Remove(card);
        }
        return cards.Select(c => c).ToList();
    }

    public bool Contains(Card card) {
        return cards.Contains(card);
    }

    public int IndexOf(Card card) {
        return cards.IndexOf(card);
    }

    public List<Card> GetAllCards() {
        return cards.Select(c => c).ToList();
    }

    public bool IsEmpty() {
        return Count == 0;
    }

    public void OrderBy<T>(Func<Card, T> selector) {
        cards = cards.OrderBy(selector).ToList();
    }
}
