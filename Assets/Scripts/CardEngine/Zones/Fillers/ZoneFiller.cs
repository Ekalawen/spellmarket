using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ZoneFiller : MonoBehaviour {

    public DeckZone poolZone;
    public Zone fillingZone;

    public void Initialize() {
        fillingZone.onRemoveCard.AddListener(OnRemoveCardOfFillingZone);
    }

    protected void OnRemoveCardOfFillingZone(Card removedCard) {
        if (!poolZone.CanDraw()) {
            return;
        }
        Card card = poolZone.Draw();
        card.MoveTo(fillingZone);
    }
}
