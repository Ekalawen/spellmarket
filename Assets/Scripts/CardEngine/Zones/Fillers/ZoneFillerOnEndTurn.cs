using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class ZoneFillerOnEndTurn : GameMonoBehaviour {

    public DeckZone poolZone;
    public Zone fillingZone;

    protected int nbCardsRemoved;

    public void Initialize() {
        nbCardsRemoved = 0;
        fillingZone.onRemoveCard.AddListener(OnRemoveCardOfFillingZone);
        game.phaseManager.onEndTurn.AddListener(OnEndTurn);
    }

    protected void OnRemoveCardOfFillingZone(Card removedCard) {
        nbCardsRemoved++;
    }

    protected void OnEndTurn() {
        for (int i = 0; i < nbCardsRemoved; i++) {
            if (!poolZone.CanDraw()) {
                return;
            }
            Card card = poolZone.Draw();
            card.MoveTo(fillingZone);
        }
        nbCardsRemoved = 0;
    }
}
