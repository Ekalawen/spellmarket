using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class FullZoneFillerWhenEmpty : GameMonoBehaviour {

    public bool shuffleOnFill = true;
    public DeckZone poolZone;
    public DeckZone fillingZone;

    public void Initialize() {
        fillingZone.onRemoveCard.AddListener(OnRemoveCardOfFillingZone);
    }

    protected void OnRemoveCardOfFillingZone(Card removedCard) {
        if(fillingZone.IsEmpty()) {
            Fill();
        }
    }

    protected void Fill() {
        List<Card> cards = poolZone.GetAllCards();
        foreach (Card card in cards) {
            card.MoveTo(fillingZone);
        }
        if (shuffleOnFill) {
            fillingZone.Shuffle();
        }
    }
}
