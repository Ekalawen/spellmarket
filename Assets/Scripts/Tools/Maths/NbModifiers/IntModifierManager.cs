using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;

public class IntModifierManager : TModifierManager<int>
{
    public IntModifierManager(MonoBehaviour holder, Func<int> getter) : base(holder, getter) {
    }
}