using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;

public class TModifierManager<T> where T : struct, IComparable<T> {

    protected List<TModifier<T>> modifiers;
    protected Func<T> getter;
    protected MonoBehaviour holder;

    [HideInInspector]
    public UnityEvent<TModifier<T>> onAddModifier = new UnityEvent<TModifier<T>>();
    [HideInInspector]
    public UnityEvent<TModifier<T>> onRemoveModifier = new UnityEvent<TModifier<T>>();
    [HideInInspector]
    public UnityEvent onChangeValue = new UnityEvent();

    public TModifierManager(MonoBehaviour holder, Func<T> getter) {
        this.holder = holder;
        this.getter = getter;
        modifiers = new List<TModifier<T>>();
    }

    public T Get() {
        T value = getter();
        return ApplyTo(value);
    }

    public T ApplyTo(T value) {
        RemoveOverModifiers();
        foreach (TModifier<T> setter in modifiers.FindAll(m => m.IsSetter())) {
            value = setter.Get();
        }
        foreach (TModifier<T> adder in modifiers.FindAll(m => m.IsAdder())) {
            value = (dynamic)value + (dynamic)adder.Get();
        }
        foreach (TModifier<T> multiplier in modifiers.FindAll(m => m.IsMultiplier())) {
            value = (dynamic)value * (dynamic)multiplier.Get();
        }
        return value;
    }

    public TModifier<T> AddModifier(TModifierMode mode, TModifierPeriod period, T value, float duration = 0) {
        return AddModifier(new TModifier<T>(mode, period, value, duration));
    }

    public TModifier<T> AddSetModifier(T value) {
        return AddModifier(TModifierMode.SET, TModifierPeriod.INFINITE, value);
    }

    public TModifier<T> AddModifier(TModifier<T> modifier) {
        if(modifier.IsSetter()) {
            RemoveAllSetterModifiers();
        }
        modifier.Initialize(this);
        modifiers.Add(modifier);
        onAddModifier.Invoke(modifier);
        onChangeValue.Invoke();
        return modifier;
    }

    protected void RemoveAllSetterModifiers() {
        List<TModifier<T>> setterModifiers = modifiers.FindAll(m => m.IsSetter());
        foreach(TModifier<T> setter in setterModifiers) {
            RemoveModifier(setter);
        }
    }

    public bool RemoveModifier(TModifier<T> modifier) {
        bool removed = modifiers.Remove(modifier);
        if (removed) {
            onRemoveModifier.Invoke(modifier);
            onChangeValue.Invoke();
        }
        return removed;
    }

    protected void RemoveOverModifiers() {
        List<TModifier<T>> toRemoveModifier = modifiers.FindAll(m => m.IsOver());
        modifiers = modifiers.FindAll(m => !m.IsOver());
        foreach(TModifier<T> modifier in toRemoveModifier) {
            onRemoveModifier.Invoke(modifier);
        }
        if(toRemoveModifier.Count > 0) {
            onChangeValue.Invoke();
        }
    }

    public List<TModifier<T>> GetModifiers() {
        return modifiers;
    }

    public float GetMultipliersCoef() {
        List<TModifier<T>> multipliers = modifiers.FindAll(m => m.IsMultiplier());
        return Enumerable.Aggregate(multipliers, 1.0f, (acc, m) => (dynamic)acc * (dynamic)m.Get());
    }

    public float GetAddedValue() {
        List<TModifier<T>> adders = modifiers.FindAll(m => m.IsAdder());
        return Enumerable.Aggregate(adders, 0.0f, (acc, m) => (dynamic)acc + (dynamic)m.Get());
    }

    public MonoBehaviour Holder() {
        return holder;
    }
}