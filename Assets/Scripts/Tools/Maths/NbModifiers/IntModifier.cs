using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyBox;

[Serializable]
public class IntModifier : TModifier<int> {
    public IntModifier(TModifierMode mode, TModifierPeriod period, int value, float duration = 0) : base(mode, period, value, duration) {
    }
}