﻿using System;

public enum TModifierMode {
    ADD,
    SET,
    MULTIPLY
};