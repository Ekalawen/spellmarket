using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyBox;

[Serializable]
public class FloatModifier : TModifier<float> {
    public FloatModifier(TModifierMode mode, TModifierPeriod period, float value, float duration = 0) : base(mode, period, value, duration) {
    }
}