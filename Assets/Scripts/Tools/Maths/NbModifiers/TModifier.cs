using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyBox;

[Serializable]
public class TModifier<T> where T : struct, IComparable<T> {

    public TModifierMode mode = TModifierMode.ADD;
    public T value;
    public TModifierPeriod period = TModifierPeriod.DURATION;
    [ConditionalField("period", false, TModifierPeriod.DURATION)]
    public float duration = 3.0f;

    protected TModifierManager<T> holder;
    protected Timer timer;

    public TModifier(TModifierMode mode, TModifierPeriod period, T value, float duration = 0) {
        this.mode = mode;
        this.value = value;
        this.period = period;
        this.duration = duration;
    }


    public TModifier(TModifier<T> other) {
        this.mode = other.mode;
        this.value = other.value;
        this.period = other.period;
        this.duration = other.duration;
    }

    public void Initialize(TModifierManager<T> manager) {
        this.holder = manager;
        if (UseDuration()) {
            timer = new Timer(duration);
            manager.Holder().StartCoroutine(CRemoveWhenDurationIsOver());
        } else {
            timer = new Timer(float.PositiveInfinity);
        }
    }

    public bool IsAdder() {
        return mode == TModifierMode.ADD;
    }

    public bool IsSetter() {
        return mode == TModifierMode.SET;
    }

    public bool IsMultiplier() {
        return mode == TModifierMode.MULTIPLY;
    }

    public bool UseDuration() {
        return period == TModifierPeriod.DURATION;
    }

    public bool UseInfiniteDuration() {
        return period == TModifierPeriod.INFINITE;
    }

    public T Get() {
        return value;
    }

    public bool IsOver() {
        return timer.IsOver();
    }

    protected void Stop() {
        timer.SetOver();
        holder.RemoveModifier(this);
    }

    public override string ToString()
    {
        string modeString = mode == TModifierMode.ADD ? "ADD" : (mode == TModifierMode.SET ? "SET" : "MULTIPLY");
        string periodString = period == TModifierPeriod.DURATION ? $"for {duration}" : "forever";
        return $"{modeString} {periodString} : {value}";
    }

    protected IEnumerator CRemoveWhenDurationIsOver()
    {
        yield return new WaitForSeconds(duration);
        Stop();
    }

    public void ChangeValue(T newValue) {
        value = newValue;
        holder.onChangeValue.Invoke();
    }
}