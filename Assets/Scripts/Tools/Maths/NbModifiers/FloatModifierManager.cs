using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;

public class FloatModifierManager : TModifierManager<float> {
    public FloatModifierManager(MonoBehaviour holder, Func<float> getter) : base(holder, getter) {
    }
}