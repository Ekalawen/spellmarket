using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class InvokableCall<T0> : BaseInvokableCall {

    protected UnityAction<T0> action;

    public InvokableCall(UnityAction<T0> action) {
        this.action = action;
    }

    public void Invoke(T0 arg0) {
        action(arg0);
    }

    public override void Invoke(object[] args) {
        if (args.Length != 1)
            throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1.");
        ThrowOnInvalidArg<T0>(args[0], 0);

        action((T0)args[0]);
    }

    public override bool Find(object targetObj, MethodInfo method) {
        return action.Target == targetObj && action.Method.Equals(method);
    }
}

public class PriorityEvent<T0> : PriorityEventBase {

    public void AddListener(UnityAction<T0> call, int priority = 0) {
        AddCall(GetCall(call), priority);
    }

    public bool RemoveListener(UnityAction<T0> call){
        return RemoveCall(call.Target, call.Method);
    }

    protected InvokableCall<T0> GetCall(UnityAction<T0> call) {
        return new InvokableCall<T0>(call);
    }

    public void Invoke(T0 arg0) {
        Invoke(new object[] { arg0 });
    }
}
