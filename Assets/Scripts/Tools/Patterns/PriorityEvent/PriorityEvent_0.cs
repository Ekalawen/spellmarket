using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class InvokableCall : BaseInvokableCall {

    protected UnityAction action;

    public InvokableCall(UnityAction action) {
        this.action = action;
    }

    public void Invoke() {
        action();
    }

    public override void Invoke(object[] args) {
        if (args.Length != 0)
            throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 0.");

        action();
    }

    public override bool Find(object targetObj, MethodInfo method) {
        // Case 827748: You can't compare Delegate.GetMethodInfo() == method, because sometimes it will not work, that's why we're using Equals instead, because it will compare that actual method inside.
        //              Comment from Microsoft:
        //              Desktop behavior regarding identity has never really been guaranteed. The desktop aggressively caches and reuses MethodInfo objects so identity checks often work by accident.
        //              .Net Native doesn�t guarantee identity and caches a lot less
        return action.Target == targetObj && action.Method.Equals(method);
    }
}

public class PriorityEvent : PriorityEventBase {

    public bool RemoveListener(UnityAction call){
        return RemoveCall(call.Target, call.Method);
    }

    protected InvokableCall GetCall(UnityAction call) {
        return new InvokableCall(call);
    }

    public void Invoke() {
        Invoke(new object[] {});
    }
}
