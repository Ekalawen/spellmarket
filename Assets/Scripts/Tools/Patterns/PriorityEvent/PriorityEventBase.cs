using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseInvokableCall {

    protected bool isZeroParams = false;

    public abstract void Invoke(object[] args);

    protected static void ThrowOnInvalidArg<T>(object arg, int index) {
        if (arg != null && !(arg is T)) {
            throw new ArgumentException($"Passed argument 'args[{index}]' is of the wrong type. Type:{arg.GetType()} Expected:{typeof(T)}");
        }
    }

    public abstract bool Find(object targetObj, MethodInfo method);

    public void SetZeroParams() {
        isZeroParams = true;
    }

    public bool IsZeroParams() {
        return isZeroParams;
    }
}

public class PriorityEventBase {

    protected Dictionary<int, List<BaseInvokableCall>> calls;

    public int Count {
        get {
            int count = 0;
            foreach (List<BaseInvokableCall> priorityList in calls.Values) {
                count += priorityList.Count;
            }
            return count;
        }
    }

    public PriorityEventBase() {
        calls = new Dictionary<int, List<BaseInvokableCall>>();
    }

    protected void AddCall(BaseInvokableCall call, int priority = 0) {
        List<BaseInvokableCall> priorityList = calls.GetValueOrDefault(priority, new List<BaseInvokableCall>());
        if (priorityList.Contains(call)) {
            Debug.LogWarning($"PriorityEventBase: Listener {call} already added in {this} !");
            return;
        }
        priorityList.Add(call);
        calls[priority] = priorityList;
    }

    protected bool RemoveCall(object targetObj, MethodInfo method) {
        foreach (List<BaseInvokableCall> priorityList in calls.Values) {
            BaseInvokableCall toRemove = priorityList.Find(c => c.Find(targetObj, method));
            if (toRemove != null) {
                priorityList.Remove(toRemove);
                return true;
            }
        }
        return false;
    }

    public void Invoke(object[] args) {
        foreach (int priorityIndex in calls.Keys.OrderBy(k => k)) {
            List<BaseInvokableCall> priorityList = calls[priorityIndex].Select(c => c).ToList();
            foreach (BaseInvokableCall call in priorityList) {
                if(call.IsZeroParams()) {
                    call.Invoke(new object[] { });
                } else {
                    call.Invoke(args);
                }
            }
        }
    }

    public void AddListener(UnityAction call, int priority = 0) {
        AddCall(GetCallNoParams(call), priority);
    }

    public bool RemoveListener(UnityAction call) {
        return RemoveCall(call.Target, call.Method);
    }

    protected InvokableCall GetCallNoParams(UnityAction call) {
        InvokableCall invokableCall = new InvokableCall(call);
        invokableCall.SetZeroParams();
        return invokableCall;
    }
}

public class PriorityEventExample {

    public static void Main() {
        PriorityEvent<int, int> priorityEvent = new PriorityEvent<int, int>();
        UnityAction<int, int> listener = new UnityAction<int, int>((a, b) => Debug.Log($"1 {a} {b}"));
        priorityEvent.AddListener(listener, 1);
        priorityEvent.AddListener((a, b) => Debug.Log($"0"), 0);
        priorityEvent.AddListener((a, b) => Debug.Log($"-1"), -1);
        priorityEvent.AddListener((a, b) => Debug.Log($"0 (default)"));
        priorityEvent.AddListener(() => Debug.Log($"3 No params !"), 3);
        priorityEvent.Invoke(1, 2);
        Debug.Log($"priorityEvent.Count = {priorityEvent.Count}");
        priorityEvent.RemoveListener(listener);
        priorityEvent.RemoveListener((a, b) => Debug.Log($"0"));
        Debug.Log($"priorityEvent.Count = {priorityEvent.Count}");

        PriorityEvent priorityEvent0 = new PriorityEvent();
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("0")), 0);
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("3")), 3);
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("1.1")), 1);
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("2")), 2);
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("1.2")), 1);
        priorityEvent0.AddListener(new UnityAction(() => Debug.Log("1.3")), 1);
        priorityEvent0.Invoke();
    }
}
