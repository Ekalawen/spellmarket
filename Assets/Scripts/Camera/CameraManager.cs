using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : FastMonoBehaviour {

    public new Camera camera;
    public float focusDistance = 1.0f;

    public void Initialize() {
    }

    public Vector3 FocusPos() {
        return camera.transform.position + camera.transform.forward * focusDistance;
    }
}
