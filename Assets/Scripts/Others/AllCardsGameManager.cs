using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

public class AllCardsGameManager : Game {

    public override void Initialize() {
        InitializeGame();
        pool.GetAllCards().ForEach(c => c.MoveTo(deck));
        deck.OrderBy(c => (int)c.color);
        deck.OrderBy(c => c.name);
        deck.OrderBy(c => IsMystic(c) * 3 + IsPermanent(c) * 2 + IsEnchantment(c) * 1);
    }

    protected int IsMystic(Card card) {
        return (card.color == CardColor.MYSTIC) ? 1 : 0;
    }

    protected int IsEnchantment(Card card) {
        return (card.type == CardType.ENCHANTMENT) ? 1 : 0;
    }

    protected int IsPermanent(Card card) {
        return (card.type == CardType.PERMANENT) ? 1 : 0;
    }

    private void InitializeGame() {
        pool.Initialize();

        deck.Initialize();
        discard.Initialize();
        river.Initialize();
        temporaryZone.Initialize();
        phaseManager.Initialize();
        turnSequencer.Initialize();
        actionManager.Initialize();
        player1.Initialize(new List<Card>());
        player2.Initialize(new List<Card>());
        playerManager.Initialize(player1, player2, turnSequencer);
        endGameChecker.Initialize();
    }
}