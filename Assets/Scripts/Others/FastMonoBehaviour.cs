using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class FastMonoBehaviour : MonoBehaviour {

    protected GameManager gm => GameManager.Instance;

    public Vector3 pos => transform.position;
    public Quaternion rot => transform.rotation;
    public Vector3 scale => transform.localScale;
}