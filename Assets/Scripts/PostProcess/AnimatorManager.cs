using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AnimatorManager : FastMonoBehaviour {

    public float cardToFocusTime = 0.35f;
    public float cardFocusTime = 0.40f;
    public float focusToZoneTime = 0.25f;
    public float directMoveTime = 0.5f;
    public float initialWaitTime = 0.1f; // time we wait before triggering another animation
    public AnimationCurve cardFocusCurve;

    protected Timer animationWaitTime;

    protected Dictionary<UI_Card, Coroutine> cardsMoves;

    public void Initialize() {
        animationWaitTime = new Timer(setOver: true);
        cardsMoves = new Dictionary<UI_Card, Coroutine>();
    }

    public void MoveCardToWithFocus(UI_Card uiCard, UI_Zone uiZone) {
        if(cardsMoves.ContainsKey(uiCard) && cardsMoves[uiCard] != null) {
            StopCoroutine(cardsMoves[uiCard]);
        }
        Coroutine coroutine = StartCoroutine(CMoveCardToWithFocus(uiCard, uiZone));
        cardsMoves[uiCard] = coroutine;
    }

    protected IEnumerator CMoveCardToWithFocus(UI_Card uiCard, UI_Zone uiZone) {
        float totalTime = TimeToMoveWithFocus();
        yield return CWaitOtherAnimations(totalTime);

        Vector3 startPos = uiCard.transform.position;
        Vector3 middlePos = gm.cameraManager.FocusPos();
        Vector3 endPos = uiZone.GetPositionFor(uiCard.card);
        bool endFaceUp = uiZone.isVisible;
        float startAngle = uiCard.IsFaceUp() ? 90 : -90;
        float endAngle = endFaceUp ? 90 : -90;

        yield return CMoveCardToFocus(uiCard, startPos, middlePos, endPos, startAngle, endAngle);

        yield return new WaitForSeconds(cardFocusTime);

        yield return CMoveCardFromFocus(uiCard, startPos, middlePos, endPos);

        cardsMoves[uiCard] = null;
    }

    public void MoveCardDirectly(UI_Card uiCard, Vector3 endPos, bool endFaceUp, float waitTime) {
        if(cardsMoves.ContainsKey(uiCard) && cardsMoves[uiCard] != null) {
            StopCoroutine(cardsMoves[uiCard]);
        }
        Coroutine coroutine = StartCoroutine(CMoveCardDirectly(uiCard, endPos, endFaceUp, waitTime));
        cardsMoves[uiCard] = coroutine;
    }

    public void MoveCardDirectly(UI_Card uiCard, UI_Zone uiZone, float waitTime) {
        Vector3 endPos = uiZone.GetPositionFor(uiCard.card);
        bool endFaceUp = uiZone.isVisible;
        MoveCardDirectly(uiCard, endPos, endFaceUp, waitTime);
    }

    protected IEnumerator CMoveCardDirectly(UI_Card uiCard, Vector3 endPos, bool endFaceUp, float waitTime = -1) {
        Vector3 startPos = uiCard.transform.position;
        float startAngle = uiCard.IsFaceUp() ? 90 : -90;
        float endAngle = endFaceUp ? 90 : -90;

        if (waitTime != -1) {
            yield return CWaitOtherAnimations(waitTime);
        }

        Timer timer = new Timer(directMoveTime);
        while (!timer.IsOver()) {
            float t = cardFocusCurve.Evaluate(timer.GetAvancement());
            uiCard.transform.position = Vector3.Lerp(startPos, endPos, t);
            float rotationAngle = Mathf.Lerp(startAngle, endAngle, t);
            uiCard.transform.rotation = Quaternion.Euler(rotationAngle, 0, 0);
            yield return null;
        }
        uiCard.transform.position = endPos;
        uiCard.TurnFace(endFaceUp);

        cardsMoves[uiCard] = null;
    }

    protected IEnumerator CMoveCardToFocus(UI_Card uiCard, Vector3 startPos, Vector3 middlePos, Vector3 endPos, float startAngle, float endAngle) {
        Timer timer = new Timer(cardToFocusTime);
        while (!timer.IsOver()) {
            float t = cardFocusCurve.Evaluate(timer.GetAvancement());
            uiCard.transform.position = Vector3.Lerp(startPos, middlePos, t);
            float rotationAngle = Mathf.Lerp(startAngle, endAngle, t);
            uiCard.transform.rotation = Quaternion.Euler(rotationAngle, 0, 0);
            yield return null;
        }
        uiCard.transform.position = middlePos;
        uiCard.TurnFaceUp();
    }

    protected IEnumerator CMoveCardFromFocus(UI_Card uiCard, Vector3 startPos, Vector3 middlePos, Vector3 endPos) {
        Timer timer = new Timer(focusToZoneTime);
        while (!timer.IsOver()) {
            float t = cardFocusCurve.Evaluate(timer.GetAvancement());
            uiCard.transform.position = Vector3.Lerp(middlePos, endPos, t);
            yield return null;
        }
        uiCard.transform.position = endPos;
    }

    protected IEnumerator CWaitOtherAnimations(float addedWaitDuration) {
        float timeToWait = GetCurrentAnimationWaitTime();
        if (timeToWait > 0) {
            animationWaitTime.AddDuree(addedWaitDuration);
        } else {
            animationWaitTime = new Timer(addedWaitDuration);
        }
        yield return new WaitForSeconds(timeToWait);
    }

    public bool IsMoving(UI_Card uiCard) {
        return cardsMoves.ContainsKey(uiCard) && cardsMoves[uiCard] != null;
    }

    public float TimeToMoveWithFocus() {
        return cardToFocusTime + cardFocusTime + focusToZoneTime;
    }

    public float GetCurrentAnimationWaitTime() {
        return Mathf.Max(0, animationWaitTime.GetRemainingTime());
    }
}
