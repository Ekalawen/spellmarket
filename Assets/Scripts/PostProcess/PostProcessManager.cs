using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;
//using UnityEngine.Rendering.PostProcessing;

public class PostProcessManager : FastMonoBehaviour {

    [Header("Cards Background color")]
    public Color attackColor;
    public Color healColor;
    public Color powerColor;
    public Color mysticColor;

    [Header("Cards Numbers colors")]
    [ColorUsage(true, true)]
    public Color textDamageColor;
    [ColorUsage(true, true)]
    public Color textHealColor;
    [ColorUsage(true, true)]
    public Color textPowerColor;
    [ColorUsage(true, true)]
    public Color textMysticColor; // Comme "Rejouez", "Combo"
    [ColorUsage(true, true)]
    public Color textEnchantementColor;

    [Header("Player text colors")]
    public Color playerDefaultColor;
    public Color playerAdditionnalTurnColor;

    [Header("Halo Materials")]
    public Material haloSelectableMaterial;
    public Material haloSelectedMaterial;

    [Header("Links")]
    public AnimatorManager animator;

    public void Initialize() {
        animator.Initialize();
    }

    public Color CardColorToColor(CardColor color) {
        switch (color) {
            case CardColor.ATTACK: return attackColor;
            case CardColor.HEAL: return healColor;
            case CardColor.POWER: return powerColor;
            case CardColor.MYSTIC: return mysticColor;
            default: return Color.magenta;
        }
    }

    public string ApplyTextColors(string text) {
        List<Tuple<string, string>> replacements = new List<Tuple<string, string>>();
        replacements.Add(new Tuple<string, string>("D�g�ts", UIHelper.SurroundWithColorWithB("D�g�ts", textDamageColor)));
        replacements.Add(new Tuple<string, string>("D�g�t", UIHelper.SurroundWithColorWithB("D�g�t", textDamageColor)));
        replacements.Add(new Tuple<string, string>("Vies", UIHelper.SurroundWithColorWithB("Vies", textHealColor)));
        replacements.Add(new Tuple<string, string>("Vie", UIHelper.SurroundWithColorWithB("Vie", textHealColor)));
        replacements.Add(new Tuple<string, string>("Puissances", UIHelper.SurroundWithColorWithB("Puissances", textPowerColor)));
        replacements.Add(new Tuple<string, string>("Puissance", UIHelper.SurroundWithColorWithB("Puissance", textPowerColor)));
        return UIHelper.ApplyReplacements(text, replacements);
    }

}
