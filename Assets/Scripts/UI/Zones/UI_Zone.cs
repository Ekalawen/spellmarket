using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class UI_Zone : FastMonoBehaviour {

    public enum Type {
        STACKED,
        IN_A_ROW,
        IN_A_SQUARE,
    }

    public bool isVisible = false;
    public Type type;
    public float cardSpacing = 0.1f; // vertical spacing between cards in a stack and horizontal spacing between cards in a row
    [ConditionalField("type", false, Type.IN_A_SQUARE)]
    public int nbCardsInARow = 5;

    protected Zone zone;

    public void Initialize(Zone zone) {
        this.zone = zone;
        gm.uiGame.RegisterUIZone(this);
        zone.onAddCard.AddListener(RecomputeCardPositions);
        zone.onRemoveCard.AddListener(RecomputeCardPositions);
    }

    public Vector3 GetPositionFor(Card card) {
        Assert.IsTrue(zone.Contains(card));
        return GetPositionFor(zone.IndexOf(card));
    }

    public Vector3 GetPositionFor(int cardIndex) {
        switch (type) {
            case Type.STACKED:
                return GetStackedPositionFor(cardIndex);
            case Type.IN_A_ROW:
                return GetInARowPositionFor(cardIndex);
            case Type.IN_A_SQUARE:
                return GetInASquarePositionFor(cardIndex);
            default:
                throw new System.Exception("Not implemented");
        }
    }

    public Vector3 GetStackedPositionFor(int cardIndex) {
        return transform.position + Vector3.up * cardSpacing * cardIndex;
    }

    public Vector3 GetInARowPositionFor(int cardIndex) {
        int nbCards = zone.Count;
        float cardWidth = UI_Card.CARD_WIDTH;
        float fullZoneWidth = cardSpacing * Mathf.Max(0, nbCards - 1) + cardWidth * Mathf.Max(0, nbCards - 1);
        Vector3 start = transform.position - Vector3.right * fullZoneWidth / 2;
        Vector3 pos = start + Vector3.right * cardIndex * (cardSpacing + cardWidth);
        return pos;
    }

    private Vector3 GetInASquarePositionFor(int cardIndex) {
        int x = cardIndex % nbCardsInARow;
        int y = cardIndex / nbCardsInARow;
        Vector3 pos = GetInARowPositionFor(x);
        int nbRows = Mathf.CeilToInt((float)zone.Count / nbCardsInARow);
        float fullZoneHeight = cardSpacing * Mathf.Max(0, nbRows - 1) + UI_Card.CARD_HEIGHT * Mathf.Max(0, nbRows - 1);
        pos -= Vector3.back * fullZoneHeight / 2;
        pos += Vector3.back * y * (cardSpacing + UI_Card.CARD_HEIGHT);
        return pos;
    }

    public Zone GetZone() {
        return zone;
    }

    protected void RecomputeCardPositions(Card dontCare) {
        RecomputeCardPositions();
    }

    protected void RecomputeCardPositions() {
        foreach(Card card in zone.GetAllCards()) {
            UI_Card uiCard = card.GetComponent<UI_Card>();
            if(uiCard.IsInNotInterruptibleAnimation()) {
                continue;
            }
            //if(uiCard.IsMoving()) {
            //    continue;
            //}
            uiCard.MoveToCurrentZonePosition(waitTime: -1);
        }
    }
}
