using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Game : FastMonoBehaviour {

    [Header("Links")]
    public UI_Pool pool;
    public UI_Zone deck;
    public UI_Zone discard;
    public UI_Zone river;
    public UI_Zone temporaryZone;
    public UI_Phase turnSequencer;
    public UI_Player player1;
    public UI_Player player2;
    public UI_EndGameChecker endGameChecker;

    [HideInInspector]
    public Game gameEngine;
    protected List<UI_Zone> allZones;

    public void Initialize(Game gameEngine) {
        this.gameEngine = gameEngine;
        allZones = new List<UI_Zone>();
        deck.Initialize(gameEngine.deck);
        discard.Initialize(gameEngine.discard);
        river.Initialize(gameEngine.river);
        temporaryZone.Initialize(gameEngine.temporaryZone);
        turnSequencer.Initialize(gameEngine.turnSequencer);
        player1.Initialize(gameEngine.player1);
        player2.Initialize(gameEngine.player2);
        pool.Initialize(gameEngine.pool);
        endGameChecker.Initialize(gameEngine.endGameChecker);
    }

    public UI_Zone GetUiZoneFor(Zone zone) {
        return allZones.Find(z => z.GetZone() == zone);
    }

    public UI_Player GetUiPlayerFor(Player player) {
        return player == player1.GetPlayer() ? player1 : player2;
    }

    public void RegisterUIZone(UI_Zone zone) {
        if (allZones.Contains(zone)) {
            return;
        }
        allZones.Add(zone);
    }
}
