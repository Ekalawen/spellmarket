using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public static class EffectFactory {

    private static Dictionary<Type, Type> map = new Dictionary<Type, Type>()
    {
        { typeof(DamageEffect), typeof(UI_DamageEffect) },
        { typeof(HealEffect), typeof(UI_HealEffect) },
        { typeof(PowerEffect), typeof(UI_PowerEffect) },
        { typeof(LosePowerEffect), typeof(UI_LosePowerEffect) },
        { typeof(PowerSetterEffect), typeof(UI_PowerSetterEffect) },
        { typeof(OnPlayEffect), typeof(UI_OnPlayEffect) },
        { typeof(OnBeginTurnEffect), typeof(UI_OnBeginTurnEffect) },
        { typeof(OnAllBeginTurnEffect), typeof(UI_OnAllBeginTurnEffect) },
        { typeof(OnAllEndTurnEffect), typeof(UI_OnAllEndTurnEffect) },
        { typeof(WhileVisibleEffect), typeof(UI_WhileVisibleEffect) },
        { typeof(CounterConditionalEffect), typeof(UI_CounterConditionalEffect) },
        { typeof(OwnEnchantmentConditionalEffect), typeof(UI_OwnEnchantmentConditionalEffect) },
        { typeof(SwapAllCardsEffect), typeof(UI_SwapAllCardsEffect) },
        { typeof(ReplayLastCardEffect), typeof(UI_ReplayLastCardEffect) },
        { typeof(CounterChangeLastTurnConditionalEffect), typeof(UI_CounterChangeLastTurnConditionalEffect) },
        { typeof(ComboConditionalEffect), typeof(UI_ComboConditionalEffect) },
        { typeof(OwnedAtBeginOfTurnConditionalEffect), typeof(UI_OwnedAtBeginOfTurnConditionalEffect) },
        { typeof(OnAnyCardPlayedEffect), typeof(UI_OnAnyCardPlayedEffect) },
        { typeof(CantModifyCounterEffect), typeof(UI_CantModifyCounterEffect) },
        { typeof(ImproveModifyCounterEffect), typeof(UI_ImproveModifyCounterEffect) },
        { typeof(CopyCounterEffect), typeof(UI_CopyCounterEffect) },
        { typeof(WhileControlledEffect), typeof(UI_WhileControlledEffect) },
        { typeof(WhileControlledAndBeginOfTurnEffect), typeof(UI_WhileControlledAndBeginOfTurnEffect) },
        { typeof(MustBeSelectedEffect), typeof(UI_MustBeSelectedEffect) },
        { typeof(CounterComparedConditionalEffect), typeof(UI_CounterComparedConditionalEffect) },
        { typeof(SelectCardEffect), typeof(UI_SelectCardEffect) },
        { typeof(SelectInTemporaryZoneEffect), typeof(UI_SelectInTemporaryZoneEffect) },
        { typeof(OnPreparePlayEffect), typeof(UI_OnPreparePlayEffect) },
        { typeof(ReplaceCardEffect), typeof(UI_ReplaceCardEffect) },
        { typeof(AddEffectToCardEffect), typeof(UI_AddEffectToCardEffect) },
        { typeof(RemoveOnPlayEffect), typeof(UI_RemoveOnPlayEffect) },
        { typeof(Swap2CardsEffect), typeof(UI_Swap2CardsEffect) },
        { typeof(ReplayEffect), typeof(UI_ReplayEffect) },
        { typeof(AddCardToZoneEffect), typeof(UI_AddCardToZoneEffect) },
        { typeof(PlayNextCardTwiceEffect), typeof(UI_PlayNextCardTwiceEffect) },
        { typeof(PlayCardsTwiceEffect), typeof(UI_PlayCardsTwiceEffect) },
        { typeof(MindControlEffect), typeof(UI_MindControlEffect) },
        { typeof(MultipleEffectsEffect), typeof(UI_MultipleEffectsEffect) },
        { typeof(ElseConditionalEffect), typeof(UI_ElseConditionalEffect) },
        { typeof(ReplaceCardWithSubEffect), typeof(UI_ReplaceCardWithSubEffect) },
        { typeof(WasEnchantConditionalEffect), typeof(UI_WasEnchantConditionalEffect) },
        { typeof(HarmoniseEffect), typeof(UI_HarmoniseEffect) },
        { typeof(CallContinuePlayEffect), typeof(UI_CallContinuePlayEffect) },
        { typeof(ResetRiverEffect), typeof(UI_ResetRiverEffect) },
        { typeof(ResetAllCardsEffect), typeof(UI_ResetAllCardsEffect) },
        { typeof(OwnCardColorConditionalEffect), typeof(UI_OwnCardColorConditionalEffect) },
        { typeof(PlaySelectedCardEffect), typeof(UI_PlaySelectedCardEffect) },
        { typeof(PlaySelectedCardsAlternativelyEffect), typeof(UI_PlaySelectedCardAlternativelyEffect) },
    };

    public static UI_Effect Create(Effect effect, GameObject gameObject)
    {
        Type uiEffectType = Map(effect);
        UI_Effect uiEffect = (UI_Effect)gameObject.AddComponent(uiEffectType);
        CreateSubEffectIfNeeded(effect, gameObject, uiEffect);
        uiEffect.Initialize(effect);
        return uiEffect;
    }

    private static Type Map(Effect effect) {
        if(!map.ContainsKey(effect.GetType())) {
            throw new Exception($"No UI_Effect for {effect.GetType()}");
        }
        return map[effect.GetType()];
    }

    private static void CreateSubEffectIfNeeded(Effect effect, GameObject gameObject, UI_Effect uiEffect) {
        if (uiEffect.GetType().IsSubclassOf(typeof(UI_EffectWithSubEffect))) {
            UI_EffectWithSubEffect uiSub = ((UI_EffectWithSubEffect)uiEffect);
            EffectWithSubEffect subEffect = (EffectWithSubEffect)effect;
            uiSub.subUiEffect = Create(subEffect.subEffect, gameObject);
        }
        if (uiEffect is UI_MultipleEffectsEffect) {
            UI_MultipleEffectsEffect uiMultiple = ((UI_MultipleEffectsEffect)uiEffect);
            MultipleEffectsEffect multipleEffect = (MultipleEffectsEffect)effect;
            uiMultiple.subUiEffects = new List<UI_Effect>();
            foreach (Effect subEffect in multipleEffect.subEffects) {
                uiMultiple.subUiEffects.Add(Create(subEffect, gameObject));
            }
        }
        if (uiEffect is UI_AddEffectToCardEffect) {
            UI_AddEffectToCardEffect uiAdd = ((UI_AddEffectToCardEffect)uiEffect);
            AddEffectToCardEffect addEffect = (AddEffectToCardEffect)effect;
            uiAdd.uiEffectToAdd = Create(addEffect.effectToAdd, gameObject);
        }
        if (uiEffect is UI_ElseConditionalEffect) {
            UI_ElseConditionalEffect uiElse = ((UI_ElseConditionalEffect)uiEffect);
            ElseConditionalEffect elseEffect = (ElseConditionalEffect)effect;
            uiElse.uiElseEffect = Create(elseEffect.elseSubEffect, gameObject);
        }
    }
}
