using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ImproveModifyCounterEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        ImproveModifyCounterEffect cantEffect = (ImproveModifyCounterEffect)effect;
        string target = ChoseByTarget(cantEffect.target, "vous", "votre adversaire", "un joueur");
        string action = ChoseByCounterDirection(cantEffect.comparator, "gagne", "perd");
        string z = (action == "gagne" && target == "vous") ? "z" : "";
        z = (action == "perd" && target == "vous") ? "ez" : z;
        string counter = ChoseByCounter(cantEffect.counterType, "Vie", "Puissance");
        Color color = GetColor(cantEffect);
        counter = UIHelper.SurroundWithColorWithB(counter, color);
        string target2 = ChoseByTarget(cantEffect.target, "vous", "votre adversaire", "ce joueur");
        string s = S(cantEffect.quantity);
        return $"Lorsque {target} {action}{z} de la {counter}, {target2} {action} {cantEffect.quantity} {counter}{s} supplémentaire{s}.";
    }

    protected Color GetColor(ImproveModifyCounterEffect cantEffect) {
        if(cantEffect.counterType == Effect.CounterType.POWER) {
            return gm.postProcessManager.textPowerColor;
        }
        return cantEffect.comparator == Effect.CounterDirection.GONE_UP ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor;
    }
}
