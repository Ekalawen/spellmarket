using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_MustBeSelectedEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        MustBeSelectedEffect mustEffect = (MustBeSelectedEffect)effect;
        string prendre = mustEffect.mustBeTaken ? "prendre" : "";
        string jouer = mustEffect.mustBePlayed ? "jouer" : "";
        string et = (mustEffect.mustBeTaken && mustEffect.mustBePlayed) ? " et " : "";
        return $"Vous devez {prendre}{et}{jouer} cette carte.";
    }
}
