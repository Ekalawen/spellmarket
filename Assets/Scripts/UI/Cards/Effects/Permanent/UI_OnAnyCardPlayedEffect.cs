using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_OnAnyCardPlayedEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        bool onlyHasBeenPlayed = ((OnAnyCardPlayedEffect)effect).onlyHasBeenSelectedThisTurn;
        string onlyHasBeenPlayedString = onlyHasBeenPlayed ? " qui n'a pas commenc� le tour devant vous" : "";
        return $"Lorsque vous jouez une carte{onlyHasBeenPlayedString}, {following}";
    }
}
