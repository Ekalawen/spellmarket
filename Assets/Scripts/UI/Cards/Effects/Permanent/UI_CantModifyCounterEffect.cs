using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_CantModifyCounterEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        CantModifyCounterEffect cantEffect = (CantModifyCounterEffect)effect;
        string target = ChoseByTarget(cantEffect.target, "Vous ne pouvez pas", "Votre adversaire ne peut pas", "Aucun joueur ne peut");
        string action = ChoseByCounterDirection(cantEffect.comparator, "gagner", "perdre");
        string counter = ChoseByCounter(cantEffect.counterType, "Vie", "Puissance");
        Color color = GetColor(cantEffect);
        counter = UIHelper.SurroundWithColorWithB(counter, color);
        return $"{target} {action} de la {counter}.";
    }

    protected Color GetColor(CantModifyCounterEffect cantEffect) {
        if(cantEffect.counterType == Effect.CounterType.POWER) {
            return gm.postProcessManager.textPowerColor;
        }
        return cantEffect.comparator == Effect.CounterDirection.GONE_UP ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor;
    }
}
