using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_CopyCounterEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        CopyCounterEffect cantEffect = (CopyCounterEffect)effect;
        string target = ChoseByTarget(cantEffect.target, "vous", "votre adversaire", "un joueur");
        string action = ChoseByCounterDirection(cantEffect.comparator, "gagne", "perd");
        string z = (action == "gagne" && target == "vous") ? "z" : "";
        z = (action == "perd" && target == "vous") ? "ez" : z;
        string counter = ChoseByCounter(cantEffect.counterType, "Vie", "Puissance");
        Color color = GetColor(cantEffect);
        counter = UIHelper.SurroundWithColorWithB(counter, color);
        string target2 = ChoseByTarget(cantEffect.target, "votre adversaire", "vous", "son adversaire");
        string action2 = ChoseByCounterDirection(cantEffect.comparator, "gagne", "perd");
        string z2 = (action2 == "gagne" && target2 == "vous") ? "z" : "";
        z2 = (action2 == "perd" && target2 == "vous") ? "ez" : z2;
        return $"Lorsque {target} {action}{z} de la {counter}, {target2} en {action2}{z2} autant.";
    }

    protected Color GetColor(CopyCounterEffect cantEffect) {
        if(cantEffect.counterType == Effect.CounterType.POWER) {
            return gm.postProcessManager.textPowerColor;
        }
        return cantEffect.comparator == Effect.CounterDirection.GONE_UP ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor;
    }
}
