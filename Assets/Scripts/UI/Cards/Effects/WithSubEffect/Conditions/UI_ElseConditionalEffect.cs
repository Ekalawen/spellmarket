using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ElseConditionalEffect : UI_EffectWithSubEffect {

    public UI_Effect uiElseEffect;

    public override string GetDescriptionSpecific() {
        ElseConditionalEffect elseEffect = (ElseConditionalEffect)effect;
        string condition = subUiEffect.GetDescriptionSpecific();
        string elseString = Uncapitalize(uiElseEffect.GetDescriptionSpecific());
        if(elseString == "") {
            return condition;
        }
        return $"{condition}\nSinon, {elseString}";
    }
}
