using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_OwnedAtBeginOfTurnConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        OwnedAtBeginOfTurnConditionalEffect condEffect = (OwnedAtBeginOfTurnConditionalEffect)effect;
        return $"Si vous poss�diez ce Sortil�ge au d�but de votre tour, {following}";
    }
}
