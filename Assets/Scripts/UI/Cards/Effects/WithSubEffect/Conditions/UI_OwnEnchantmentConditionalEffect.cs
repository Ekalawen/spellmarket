using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_OwnEnchantmentConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        OwnEnchantmentConditionalEffect condEffect = (OwnEnchantmentConditionalEffect)effect;
        string enchantement = UIHelper.SurroundWithColorWithB("Enchantement", gm.postProcessManager.textEnchantementColor);
        if (condEffect.shouldOwn) {
            string target = ChoseByTarget(condEffect.target, "vous poss�dez", "votre adversaire poss�de", "les deux joueurs poss�dent");
            return $"Si {target} un {enchantement}, {following}";
        } else {
            string target = ChoseByTarget(condEffect.target, "vous ne poss�dez pas", "votre adversaire ne poss�de pas", "aucun joueur ne poss�de");
            return $"Si {target} d'{enchantement}, {following}";
        }
    }
}
