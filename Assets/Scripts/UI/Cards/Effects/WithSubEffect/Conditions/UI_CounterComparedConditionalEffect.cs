using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_CounterComparedConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        CounterComparedConditionalEffect condEffect = (CounterComparedConditionalEffect)effect;
        string target = ChoseByTarget(condEffect.target, "vous avez", "votre adversaire a", "les deux joueurs ont");
        string comparator = ChoseByComparator(condEffect.comparator, "plus ou autant", "moins ou autant", "strictement plus", "strictement moins", "exactement autant");
        string counter = ChoseByCounter(condEffect.counter, "Vie", "Puissance");
        counter = UIHelper.SurroundWithColorWithB(counter, GetColorType());
        string target2 = ChoseByTarget(condEffect.target, "votre adversaire", "vous", "l'autre");
        return $"Si {target} {comparator} de {counter} que {target2}, {following}";
    }
}
