using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_CounterChangeLastTurnConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        CounterChangeLastTurnConditionalEffect condEffect = (CounterChangeLastTurnConditionalEffect)effect;
        string target = ChoseByTarget(condEffect.target, "vous avez", "votre adversaire a", "les deux joueurs ont");
        string comparator = condEffect.comparator == CounterChangeLastTurnConditionalEffect.CounterDirection.GONE_UP ? "gagn�" : "perdu";
        string counter = condEffect.counter == ConditionalEffect.CounterType.HEALTH ? "Vie" : "Puissance";
        Color color = GetColor(condEffect);
        string coloredPart = UIHelper.SurroundWithColorWithB($"{comparator} de la {counter}", color);
        return $"Si {target} {coloredPart} au tour pr�c�dent, {following}";
    }

    protected Color GetColor(CounterChangeLastTurnConditionalEffect condEffect) {
        if(condEffect.counter == CounterConditionalEffect.CounterType.POWER) {
            return gm.postProcessManager.textPowerColor;
        }
        return condEffect.comparator == CounterChangeLastTurnConditionalEffect.CounterDirection.GONE_UP ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor;
    }
}
