using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_WasEnchantConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        string shouldBe = ((WasEnchantConditionalEffect)effect).shouldBe ? "c'�tait" : "ce n'�tait pas";
        string enchantement = UIHelper.SurroundWithColorWithB("Enchantement", gm.postProcessManager.textEnchantementColor);
        return $"Si {shouldBe} un {enchantement}, {following}";
    }
}
