using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ComboConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        ComboConditionalEffect comboEffect = (ComboConditionalEffect)effect;
        string combo = UIHelper.SurroundWithColorWithB("Combo", gm.postProcessManager.textMysticColor);
        return $"{combo} : Si vous poss�dez une autre carte {combo}, {following}";
    }
}
