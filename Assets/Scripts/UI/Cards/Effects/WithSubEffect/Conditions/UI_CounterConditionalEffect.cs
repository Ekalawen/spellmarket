using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_CounterConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        CounterConditionalEffect condEffect = (CounterConditionalEffect)effect;
        string target = ChoseByTarget(condEffect.target, "vous avez", "votre adversaire a", "les deux joueurs ont");
        string counter = condEffect.counter == CounterConditionalEffect.CounterType.HEALTH ? "Vie" : "Puissance";
        string s = condEffect.treshold >= 2 ? "s" : "";
        string comparator = ChoseByComparator(condEffect.comparator, condEffect.treshold.ToString());
        Color color = GetColor(condEffect);
        string coloredCounter = UIHelper.SurroundWithColorWithB($"{condEffect.treshold} {counter}{s}", color);
        return $"Si {target} {coloredCounter} {comparator}, {following}";
    }

    protected Color GetColor(CounterConditionalEffect condEffect) {
        if(condEffect.counter == CounterConditionalEffect.CounterType.POWER) {
            return gm.postProcessManager.textPowerColor;
        }
        return condEffect.target == BasicEffect.Target.SELF ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor;
    }
}
