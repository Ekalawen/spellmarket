using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_OwnCardColorConditionalEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        OwnCardColorConditionalEffect condEffect = (OwnCardColorConditionalEffect)effect;
        string target = ChoseByTarget(condEffect.target, "vous poss�dez", "votre adversaire poss�de", "chaque joueur poss�de");
        string colorString = ChoseByColor(condEffect.color, "Rouge", "Verte", "Bleu", "Violette");
        Color color = GetColorFor(condEffect.color);
        colorString = UIHelper.SurroundWithColorWithB(colorString, color);
        string autre = condEffect.otherCard ? " autre" : "";
        return $"Si {target} une{autre} carte {colorString}, {following}";
    }

    protected Color GetColorFor(CardColor color) {
        switch (color) {
            case CardColor.ATTACK: return gm.postProcessManager.textDamageColor;
            case CardColor.HEAL: return gm.postProcessManager.textHealColor;
            case CardColor.POWER: return gm.postProcessManager.textPowerColor;
            case CardColor.MYSTIC: return gm.postProcessManager.textMysticColor;
            default: return Color.white;
        }
    }
}
