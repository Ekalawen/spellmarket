using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_OnAllBeginTurnEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        return subUiEffect.GetDescriptionSpecific();
    }
}
