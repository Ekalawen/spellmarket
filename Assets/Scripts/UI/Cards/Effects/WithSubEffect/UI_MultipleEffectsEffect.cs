using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_MultipleEffectsEffect : UI_Effect {

    public List<UI_Effect> subUiEffects = new List<UI_Effect>();

    public override string GetDescriptionSpecific() {
        string description = "";
        foreach (UI_Effect subUiEffect in subUiEffects) {
            if(subUiEffect == subUiEffects.First()) {
                description += subUiEffect.GetDescriptionSpecific();
            } else {
                description += Uncapitalize(subUiEffect.GetDescriptionSpecific());
            }
            if(subUiEffect != subUiEffects.Last()) {
                description = RemoveLastDot(description);
                string join = subUiEffect == subUiEffects[subUiEffects.Count - 2] ? " et " : ", ";
                description += join;
            }
        }
        return description;
    }
}
