using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_EffectWithSubEffect : UI_Effect {

    public UI_Effect subUiEffect;

    public override void Initialize(Effect effect) {
        base.Initialize(effect);
    }

    public override string GetDescriptionSpecific() {
        return subUiEffect.GetDescriptionSpecific();
    }
}
