using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_ReplayEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        ReplayEffect replayEffect = (ReplayEffect)effect;
        return ChoseByTarget(replayEffect.target,
            UIHelper.SurroundWithColorWithB("Rejouez.", gm.postProcessManager.textMysticColor),
            $"Votre adversaire joue un {UIHelper.SurroundWithColorWithB("tour supplémentaire", gm.postProcessManager.textMysticColor)}.", "");
    }
}
