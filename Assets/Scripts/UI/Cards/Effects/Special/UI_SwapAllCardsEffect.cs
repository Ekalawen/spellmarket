using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_SwapAllCardsEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        return $"�changez toutes vos cartes avec toutes les cartes adverses.";
    }
}
