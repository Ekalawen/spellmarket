using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_PlayCardsTwiceEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        string joueesDeuxFois = UIHelper.SurroundWithColorWithB("jou�es deux fois", gm.postProcessManager.textMysticColor);
        return $"Les cartes que vous jouez sont {joueesDeuxFois}.";
    }
}
