using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_MindControlEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        string rejouez = UIHelper.SurroundWithColorWithB("rejouez", gm.postProcessManager.textMysticColor);
        return $"Vous jouez le prochain tour de votre adversaire.\nPuis vous {rejouez}.";
    }
}
