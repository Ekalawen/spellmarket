using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_ReplayLastCardEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        return $"Rejouez la derni�re carte jou�e.";
    }
}
