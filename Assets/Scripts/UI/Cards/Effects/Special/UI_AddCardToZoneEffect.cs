using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_AddCardToZoneEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        AddCardToZoneEffect addEffect = (AddCardToZoneEffect)effect;
        string zone = ChoseByZoneType(addEffect.zoneType, "� votre main", "� la main de votre adversaire", "� la rivi�re", "aux mains de chaque joueur");
        string ajoutezUneCarte = UIHelper.SurroundWithColorWithB("Ajoutez une carte", gm.postProcessManager.textMysticColor);
        return $"{ajoutezUneCarte} de la pioche {zone}.";
    }
}
