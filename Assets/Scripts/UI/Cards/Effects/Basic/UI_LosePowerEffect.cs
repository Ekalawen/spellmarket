using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_LosePowerEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        LosePowerEffect powerEffect = (LosePowerEffect)effect;
        int loss = powerEffect.quantity;
        string surroundedPart = $"{loss} Puissance{S(loss)}";
        string target = ChoseByTarget(powerEffect.target, "Perdez", "Votre adversaire perd", "Chaque joueur perd");
        return $"{target} {UIHelper.SurroundWithColorWithB(surroundedPart, gm.postProcessManager.textPowerColor)}.";
    }
}
