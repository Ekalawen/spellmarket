using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.LookDev;
using UnityEngine.UI;

public class UI_PowerSetterEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        PowerSetterEffect powerEffect = (PowerSetterEffect)effect;
        int quantity = powerEffect.quantity;
        string s = quantity >= 2 ? "s" : "";
        string target = ChoseByTarget(powerEffect.target, "Vous passez", "Votre adversaire passe", "Chaque joueur passe");
        string coloredCounter = UIHelper.SurroundWithColorWithB($"{quantity} Puissance{s}", gm.postProcessManager.textPowerColor);
        return $"{target} � {coloredCounter}.";
    }
}
