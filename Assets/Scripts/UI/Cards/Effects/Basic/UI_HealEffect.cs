using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_HealEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        HealEffect healEffect = (HealEffect)effect;
        int heal = healEffect.quantity;
        string surroundedPart = $"{heal} Vie{S(heal)}";
        string target = ChoseByTarget(healEffect.target, "Gagnez", "Votre adversaire gagne", "Chaque joueur gagne");
        return $"{target} {UIHelper.SurroundWithColorWithB(surroundedPart, gm.postProcessManager.textHealColor)}.";
    }
}
