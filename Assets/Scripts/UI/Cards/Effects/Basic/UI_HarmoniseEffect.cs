using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_HarmoniseEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        HarmoniseEffect hEffect = (HarmoniseEffect)effect;
        string verbe = ChoseByTarget(hEffect.baseTarget,
            hEffect.baseComparator == Effect.Comparator.STRICTLY_GREATER ? "Perdez" : "Gagnez",
            hEffect.baseComparator == Effect.Comparator.STRICTLY_GREATER ? "Votre adversaire perd" : "Votre adversaire gagne",
            "");
        string counter = ChoseByCounter(hEffect.baseCounterType, "Vie", "Puissance");
        Color counterColor = GetColorType();
        counter = UIHelper.SurroundWithColorWithB(counter, counterColor);
        string opponent = ChoseByTarget(hEffect.baseTarget, "votre adversaire", "vous", "");
        string verbe2 = ChoseByTarget(hEffect.harmoniseTarget,
          hEffect.harmoniseDirection == Effect.CounterDirection.GONE_UP ? "Vous gagnez" : "Vous perdez",
          hEffect.harmoniseDirection == Effect.CounterDirection.GONE_UP ? "Votre adversaire gagne" : "Votre adversaire perd",
          "");
        string counter2 = ChoseByCounter(hEffect.harmoniseCounterType, "Vie", "Puissance");
        Color counter2Color = hEffect.harmoniseCounterType == Effect.CounterType.POWER ? gm.postProcessManager.textPowerColor
            : (hEffect.harmoniseDirection == Effect.CounterDirection.GONE_UP ? gm.postProcessManager.textHealColor : gm.postProcessManager.textDamageColor);
        counter2 = UIHelper.SurroundWithColorWithB(counter2, counter2Color);
        return $"{verbe} de la {counter} jusqu'� en avoir autant que {opponent}. {verbe2} autant de {counter2}.";
    }
}
