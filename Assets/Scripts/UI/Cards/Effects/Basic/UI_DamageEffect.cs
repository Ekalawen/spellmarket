using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_DamageEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        DamageEffect damageEffect = (DamageEffect)effect;
        int damage = damageEffect.quantity;
        string surroundedPart = $"{damage} D�g�t{S(damage)}";
        string target = ChoseByTarget(damageEffect.target, "Vous vous infligez", "Infligez", "Infligez � chaque joueurs");
        return $"{target} {UIHelper.SurroundWithColorWithB(surroundedPart, gm.postProcessManager.textDamageColor)}.";
    }
}
