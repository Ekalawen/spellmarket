using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_PowerEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        PowerEffect powerEffect = (PowerEffect)effect;
        int gain = powerEffect.quantity;
        string surroundedPart = $"{gain} Puissance{S(gain)}";
        string target = ChoseByTarget(powerEffect.target, "Gagnez", "Votre adversaire gagne", "Chaque joueur gagne");
        return $"{target} {UIHelper.SurroundWithColorWithB(surroundedPart, gm.postProcessManager.textPowerColor)}.";
    }
}
