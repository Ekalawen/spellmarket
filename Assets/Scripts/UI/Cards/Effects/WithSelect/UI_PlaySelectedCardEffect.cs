using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_PlaySelectedCardEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        return $"Jouez-la.";
    }
}
