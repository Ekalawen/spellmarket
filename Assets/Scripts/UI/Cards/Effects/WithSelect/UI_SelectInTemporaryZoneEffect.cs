using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_SelectInTemporaryZoneEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        SelectInTemporaryZoneEffect selectEffect = effect as SelectInTemporaryZoneEffect;
        int nbCards = selectEffect.nbCardsToDraw;
        string s = nbCards > 1 ? "s" : "";
        string following = RemoveLastDot(Uncapitalize(subUiEffect.GetDescriptionSpecific()));
        int nbSelectedCards = selectEffect.zones.Count;
        string nbSelectedCardsString = nbSelectedCards == 1 ? "une" : nbSelectedCards.ToString();
        string lesAutres = nbSelectedCards == nbCards - 1 ? "l'autre" : "les autres";
        string followingString = nbCards == nbSelectedCards ? $"{following}." : $"{following}, puis d�faussez {lesAutres}.";
        return $"Piochez {nbCards} carte{s} et choisissez-en {nbSelectedCardsString}, {followingString}";
    }
}
