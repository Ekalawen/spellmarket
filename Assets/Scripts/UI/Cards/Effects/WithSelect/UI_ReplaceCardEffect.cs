using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ReplaceCardEffect : UI_Effect {

    public override string GetDescriptionSpecific() {
        ReplaceCardEffect replaceEffect = effect as ReplaceCardEffect;
        if(replaceEffect.type == ReplaceCardEffect.ReplaceCardEffectType.Select) {
            return "D�faussez la premi�re et remplacez-l� par la deuxi�me.";
        } else {
            return "D�faussez-l� et remplacez-l� par une carte de la pioche.";
        }
    }
}
