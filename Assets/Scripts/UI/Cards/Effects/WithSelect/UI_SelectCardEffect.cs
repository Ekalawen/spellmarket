using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_SelectCardEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = Uncapitalize(subUiEffect.GetDescriptionSpecific());
        SelectCardEffect selectEffect = (SelectCardEffect)effect;
        string zones = "";
        foreach (SelectCardEffect.ZoneType zone in selectEffect.zones) {
            zones += ChoseByZoneType(zone, "de vos cartes", "carte adverse", "carte de la rivi�re", "carte d'un joueur");
            if(zone != selectEffect.zones.Last()) {
                zones += " et une ";
            }
        }
        return $"Choisissez une {zones}, {following}";
    }
}
