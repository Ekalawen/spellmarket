using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_AddEffectToCardEffect : UI_Effect {

    public UI_Effect uiEffectToAdd;

    public override string GetDescriptionSpecific() {
        string following = UIHelper.SurroundWithColorWithoutB(uiEffectToAdd.GetDescriptionSpecific(), gm.postProcessManager.textMysticColor);
        return $"y ajoute l'effet \"{following}\".";
    }
}
