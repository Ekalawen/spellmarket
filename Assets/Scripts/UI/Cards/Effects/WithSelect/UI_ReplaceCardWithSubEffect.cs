using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ReplaceCardWithSubEffect : UI_EffectWithSubEffect {

    public override string GetDescriptionSpecific() {
        string following = subUiEffect.GetDescriptionSpecific();
        ReplaceCardWithSubEffect replaceEffect = effect as ReplaceCardWithSubEffect;
        if(replaceEffect.type == ReplaceCardEffect.ReplaceCardEffectType.Select) {
            return $"D�faussez la premi�re et remplacez-l� par la deuxi�me.\n{following}";
        } else {
            return $"D�faussez-l� et remplacez-l� par une carte de la pioche.\n{following}";
        }
    }
}
