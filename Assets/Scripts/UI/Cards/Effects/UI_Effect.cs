using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class UI_Effect : FastMonoBehaviour {

    protected Effect effect;

    public virtual void Initialize(Effect effect) {
        this.effect = effect;
    }

    public abstract string GetDescriptionSpecific();

    public string GetDescription() {
        string description = GetDescriptionSpecific();
        //description = gm.postProcessManager.ApplyTextColors(description);
        if(IsAdded()) {
            description = UIHelper.SurroundWithColorWithoutB(description, gm.postProcessManager.textMysticColor);
        }
        return description;
    }

    protected string S(int quantity) {
        return quantity >= 2 ? "s" : "";
    }

    protected string ChoseByTarget(Effect.Target target, string self, string opponent, string both) {
        switch (target) {
            case Effect.Target.SELF: return self;
            case Effect.Target.OPPONENT: return opponent;
            case Effect.Target.BOTH: return both;
            default: return "";
        }
    }

    protected string ChoseByColor(CardColor color, string rouge, string vert, string bleu, string violet) {
        switch (color) {
            case CardColor.ATTACK: return rouge;
            case CardColor.HEAL: return vert;
            case CardColor.POWER: return bleu;
            case CardColor.MYSTIC: return violet;
            default: return "";
        }
    }

    protected string ChoseByCounter(Effect.CounterType counter, string health, string power) {
        switch (counter) {
            case Effect.CounterType.HEALTH: return health;
            case Effect.CounterType.POWER: return power;
            default: return "";
        }
    }

    protected string ChoseByCounterDirection(Effect.CounterDirection comparator, string up, string down) {
        switch (comparator)
        {
            case Effect.CounterDirection.GONE_UP: return up;
            case Effect.CounterDirection.GONE_DOWN: return down;
            default: return "";
        }
    }

    protected string ChoseByComparator(Effect.Comparator comparator, string autantOuPlus, string autantOuMoins, string strictPlus, string strictMoins, string autant) {
        switch (comparator) {
            case Effect.Comparator.GREATER_OR_EQUAL: return autantOuPlus;
            case Effect.Comparator.LESSER_OR_EQUAL: return autantOuMoins;
            case Effect.Comparator.STRICTLY_GREATER: return strictPlus;
            case Effect.Comparator.STRICTLY_LESSER: return strictMoins;
            case Effect.Comparator.EQUAL: return autant;
            default: return "";
        }
    }

    protected string ChoseByComparator(Effect.Comparator comparator, string value) {
        switch (comparator) {
            case Effect.Comparator.GREATER_OR_EQUAL: return $"{value} ou plus";
            case Effect.Comparator.LESSER_OR_EQUAL: return $"{value} ou moins";
            case Effect.Comparator.STRICTLY_GREATER: return $"strictement plus de {value}";
            case Effect.Comparator.STRICTLY_LESSER: return $"strictement moins de {value}";
            case Effect.Comparator.EQUAL: return $"exactement {value}";
            default: return "";
        }
    }

    public UI_Card GetUiCard() {
        return effect.GetCard().GetComponent<UI_Card>();
    }

    public Color GetColorType() {
        switch (effect.GetCard().color) {
            case CardColor.ATTACK: return gm.postProcessManager.textDamageColor;
            case CardColor.HEAL: return gm.postProcessManager.textHealColor;
            case CardColor.POWER: return gm.postProcessManager.textPowerColor;
            case CardColor.MYSTIC: return gm.postProcessManager.textMysticColor;
            default: return Color.magenta;
        }
    }

    protected string ChoseByZoneType(SelectCardEffect.ZoneType zoneType, string myHand, string opponentHand, string river, string bothHands) {
        switch (zoneType) {
            case SelectCardEffect.ZoneType.SELF_HAND: return myHand;
            case SelectCardEffect.ZoneType.OPPONENT_HAND: return opponentHand;
            case SelectCardEffect.ZoneType.RIVER: return river;
            case SelectCardEffect.ZoneType.BOTH_HANDS: return bothHands;
            default: return "";
        }
    }

    public string Uncapitalize(string text) {
        Match match = Regex.Match(text, pattern: @"(?<!<[^>]*)[A-Z�����������ܟ�](?![^<]*>)");
        if (match.Success) {
            return text.Substring(0, match.Index) + text.Substring(match.Index, 1).ToLower() + text.Substring(match.Index + 1);
        }
        return text;
    }

    public string RemoveLastDot(string text) {
        int index = text.LastIndexOf(".");
        if(index == -1) return text;
        return text.Substring(0, index);
    }

    public bool IsAdded() {
        return effect.IsAdded();
    }
}