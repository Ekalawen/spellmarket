using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_Pool : FastMonoBehaviour {

    public bool useNotInGameInitialization = false;

    protected Pool pool;

    public void Initialize(Pool pool)
    {
        this.pool = pool;
        InitializeCardsOfPool(pool);
    }

    protected void InitializeCardsOfPool(Pool pool) {
        this.pool.onInstantiateCard.AddListener(OnInstantiateCard);
        foreach (Card card in pool.InstantiatedCards()) {
            OnInstantiateCard(card);
        }
    }

    protected void OnInstantiateCard(Card card) {
        UI_Card uiCard = card.GetComponent<UI_Card>();
        if (useNotInGameInitialization) {
            uiCard.InitializeNotInGame();
        } else {
            uiCard.Initialize();
        }
    }

    public List<UI_Card> GetAllUiCards() {
        return pool.InstantiatedCards().Select(card => card.GetComponent<UI_Card>()).ToList();
    }
}
