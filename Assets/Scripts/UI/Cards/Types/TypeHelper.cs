using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public static class TypeHelper
{
    public static Color TypeToColor(CardType type) {
        switch (type) {
            case CardType.SPELL:
                return Color.black;
            case CardType.ENCHANTMENT:
                return Color.white;
            case CardType.PERMANENT:
                return Color.gray;
            default:
                return Color.magenta;
        }
    }

    public static string TypeToName(CardType type)
    {
        switch (type)
        {
            case CardType.SPELL:
                return "Sortil�ge";
            case CardType.ENCHANTMENT:
                return "Enchantement";
            case CardType.PERMANENT:
                return "Permanent";
            default:
                return "Erreur ;)";
        }
    }

    public static Color EffectTypeToColor(UI_EffectType effectType) {
        switch (effectType) {
            case UI_EffectType.ATTACK:
                return Color.red;
            case UI_EffectType.HEAL:
                return Color.green;
            case UI_EffectType.POWER:
                return Color.blue;
            case UI_EffectType.MYSTIC:
                return new Color(173, 71, 167); // Violet
            default:
                return Color.magenta;
        }
    }
}