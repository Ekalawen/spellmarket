using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UI_EffectType {
    ATTACK,
    HEAL,
    POWER,
    MYSTIC,
}
