using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_Card : FastMonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public static float CARD_WIDTH = 0.635f;
    public static float CARD_HEIGHT = 0.88f;

    [Header("Parameters")]
    public new string name;
    public Sprite sprite;
    public Card card;

    [Header("Links")]
    public TMP_Text nameText;
    public TMP_Text typeText;
    public TMP_Text descriptionText;
    public Image image;
    public Renderer halo;
    public List<Image> backgroundImages;

    protected bool isFaceUp;
    protected List<UI_Effect> uiEffects;
    protected bool isCameraFocused = false;
    protected BoolTimer inNotInterruptibleAnimation;

    public void Initialize() {
        inNotInterruptibleAnimation = new BoolTimer(this);
        InitializeUIEffects();
        FillCardUI();
        TurnFaceUp();
        InitializeHalo();
        TeleportAboveCamera();
        MoveToCurrentZonePositionWithInitialTime();
        card.onMove.AddListener(OnCardMove);
        card.onNotPlayTriggered.AddListener(OnCardNotPlayTriggered);
        card.onChangeEffects.AddListener(OnChangeEffects);
    }

    public void InitializeNotInGame() {
        InitializeUIEffects();
        FillCardUI();
        TurnFaceUp();
        InitializeHaloNotInGame();
        TeleportToZonePosition();
    }

    protected void InitializeUIEffects() {
        uiEffects = new List<UI_Effect>();
        foreach(Effect effect in card.effects) {
            UI_Effect uiEffect = EffectFactory.Create(effect, gameObject);
            uiEffects.Add(uiEffect);
        }
    }

    protected void FillCardUI() {
        nameText.text = name;
        typeText.text = TypeHelper.TypeToName(card.type);
        descriptionText.text = GetDescription();
        image.sprite = sprite;
        foreach (Image backgroundImage in backgroundImages) {
            backgroundImage.color = gm.postProcessManager.CardColorToColor(GetColor());
        }
        SetEnchantmentStyle();
        SetPermanentStyle();
    }

    private CardColor GetColor() {
        return card.color;
    }

    private void OnChangeEffects() {
        foreach(UI_Effect uiEffect in uiEffects) {
            Destroy(uiEffect);
        }
        InitializeUIEffects();
        FillCardUI();
    }

    protected void SetEnchantmentStyle() {
        if(card.type == CardType.ENCHANTMENT) {
            backgroundImages[3].color = new Color(0.9f, 0.9f, 0.9f);
            typeText.color = Color.black;
        }
    }

    protected void SetPermanentStyle() {
        if(card.type == CardType.PERMANENT) {
            backgroundImages[3].color = Color.black;
        }
    }

    public string GetDescription() {
        string description = "";
        foreach (UI_Effect uiEffect in uiEffects) {
            description += uiEffect.GetDescription() + "\n";
        }
        return description.Substring(0, description.Length - 1);
    }

    public void TurnFace(bool visible) {
        if (visible) {
            TurnFaceUp();
        } else {
            TurnFaceDown();
        }
    }

    public void TurnFaceUp() {
        transform.rotation = Quaternion.Euler(90, 0, 0);
        isFaceUp = true;
    }

    public void TurnFaceDown() {
        transform.rotation = Quaternion.Euler(-90, 0, 0);
        isFaceUp = false;
    }

    public bool IsFaceUp() {
        return isFaceUp;
    }

    public UI_Zone CurrentUIZone() {
        return gm.uiGame.GetUiZoneFor(card.CurrentZone());
    }

    public void TeleportToZonePosition() {
        transform.position = CurrentUIZone().GetPositionFor(card);
        TurnFace(CurrentUIZone().isVisible);
    }

    protected void TeleportAboveCamera() {
        transform.position = gm.uiGame.deck.transform.position + Vector3.up * 3;
        TurnFaceDown();
    }

    public void MoveToCurrentZonePosition(float waitTime) {
        gm.postProcessManager.animator.MoveCardDirectly(this, CurrentUIZone(), waitTime);
    }

    protected void MoveToCurrentZonePositionWithInitialTime() {
        MoveToCurrentZonePosition(gm.postProcessManager.animator.initialWaitTime);
    }

    protected void MoveToSmallFocus() {
        Vector3 currentPos = CurrentUIZone().GetPositionFor(card);
        Vector3 cameraDirection = (gm.cameraManager.camera.transform.position - currentPos).normalized;
        Vector3 focusPos = currentPos + cameraDirection * 1.5f;
        gm.postProcessManager.animator.MoveCardDirectly(this, focusPos, endFaceUp: true, waitTime: -1);
    }

    protected void MoveToCameraFocus(bool waitToRemovePreviousFocus) {
        float waitTime = waitToRemovePreviousFocus ? gm.postProcessManager.animator.TimeToMoveWithFocus() : -1;
        // Remove other focused cards :)
        foreach (UI_Card otherCard in gm.uiGame.pool.GetAllUiCards()) {
            if (otherCard != this) {
                otherCard.MoveToCurrentZonePosition(waitTime);
                otherCard.isCameraFocused = false;
            }
        }
        Vector3 focusPos = gm.cameraManager.FocusPos();
        gm.postProcessManager.animator.MoveCardDirectly(this, focusPos, endFaceUp: true, waitTime);
    }

    protected void OnCardMove(Zone previousZone, Zone nextZone, bool isPlayMove) {
        if (!isPlayMove) {
            float waitTime = previousZone == gm.uiGame.discard.GetZone() ? gm.postProcessManager.animator.initialWaitTime : -1;
            //float waitTime = gm.postProcessManager.animator.directMoveTime;
            MoveToCurrentZonePosition(waitTime);
        } else {
            OnPlayCard();
        }
    }

    protected void OnPlayCard() {
        float animationWaitTime = gm.postProcessManager.animator.GetCurrentAnimationWaitTime();
        inNotInterruptibleAnimation.AddTime(gm.postProcessManager.animator.TimeToMoveWithFocus() + animationWaitTime);
        gm.postProcessManager.animator.MoveCardToWithFocus(this, CurrentUIZone());
    }

    protected void OnCardNotPlayTriggered() {
        OnPlayCard();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if(inNotInterruptibleAnimation.value) {
            return;
        }
        if (isCameraFocused)
        {
            return;
        }
        MoveToSmallFocus();
    }

    public void OnPointerExit(PointerEventData eventData) {
        if(inNotInterruptibleAnimation.value) {
            return;
        }
        if (isCameraFocused && IsMoving()) {
            return;
        }
        MoveToCurrentZonePosition(waitTime: -1);
        isCameraFocused = false;
    }

    public bool IsMoving() {
        return gm.postProcessManager.animator.IsMoving(this);
    }

    protected void OnPointerClickRight() {
        if(isCameraFocused) {
            isCameraFocused = false;
            MoveToCurrentZonePosition(waitTime: -1);
        } else {
            isCameraFocused = true;
            MoveToCameraFocus(waitToRemovePreviousFocus: false);
        }
    }

    protected void DisableHalo() {
        halo.gameObject.SetActive(false);
    }

    protected void SetSelectableHalo() {
        halo.gameObject.SetActive(true);
        halo.material = gm.postProcessManager.haloSelectableMaterial;
    }

    protected void SetSelectedHalo() {
        halo.gameObject.SetActive(true);
        halo.material = gm.postProcessManager.haloSelectedMaterial;
    }

    protected void InitializeHalo() {
        InitializeHaloNotInGame();
        card.onStartSelectable.AddListener(SetSelectableHalo);
        card.onStopSelectable.AddListener(DisableHalo);
        card.onPreparePlay.AddListener(SetSelectedHalo);
        card.onPlay.AddListener(DisableHalo);
    }

    protected void InitializeHaloNotInGame() {
        if(card.IsSelectable()) {
            SetSelectableHalo();
        } else {
            DisableHalo();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            OnPointerClickLeft();
        }
        else if (eventData.button == PointerEventData.InputButton.Right) {
            OnPointerClickRight();
        }
    }

    protected void OnPointerClickLeft() {
        if (!card.IsSelectable()) {
            return;
        }
        CardAction action = Game.Instance.actionManager.GetCurrentCardAction();
        action.SelectCard(card);
        action.Perform();
    }

    public bool IsInNotInterruptibleAnimation() {
        return inNotInterruptibleAnimation.value;
    }
}
