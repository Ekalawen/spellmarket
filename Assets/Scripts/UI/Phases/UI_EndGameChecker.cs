using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_EndGameChecker : FastMonoBehaviour {

    protected EndGameChecker endGameChecker;

    public void Initialize(EndGameChecker endGameChecker) {
        this.endGameChecker = endGameChecker;
        endGameChecker.onWin.AddListener(OnWin);
        endGameChecker.onTie.AddListener(OnTie);
    }

    protected void OnWin(Player winningPlayer) {
        UI_Player uiWinningPlayer = gm.uiGame.GetUiPlayerFor(winningPlayer);
        uiWinningPlayer.SetWinningHint();
        Player otherPlayer = Game.Instance.playerManager.GetOtherPlayer(winningPlayer);
        UI_Player uiLosingPlayer = gm.uiGame.GetUiPlayerFor(otherPlayer);
        uiLosingPlayer.SetLosingHint();
    }

    protected void OnTie() {
        Debug.Log($"TIE !");
        gm.uiGame.player1.SetTieHint();
        gm.uiGame.player2.SetTieHint();
    }

    public bool IsGameOver() {
        return endGameChecker.IsGameOver();
    }
}
