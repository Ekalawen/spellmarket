using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Counter : FastMonoBehaviour {

    public GameObject tokenPrefab;
    public Transform container;
    public float alphaDisabled = 0.1f;

    protected Counter counter;
    protected List<GameObject> tokens;

    public void Initialize(Counter counter) {
        this.counter = counter;
        tokens = new List<GameObject>();
        RepopulateTokens();
        this.counter.onChange.AddListener(RepopulateTokens);
    }

    public void RepopulateTokens(int variation = 0) {
        foreach (GameObject token in tokens) {
            Destroy(token);
        }
        int nbTokens = counter.GetValue();
        for (int i = 0; i < nbTokens; i++) {
            GameObject token = Instantiate(tokenPrefab, container);
            tokens.Add(token);
        }
        int nbDisabledTokens = counter.maxValue - nbTokens;
        for (int i = 0; i < nbDisabledTokens; i++) {
            GameObject token = Instantiate(tokenPrefab, container);
            Image image = token.GetComponent<Image>();
            image.color = ColorsTools.SetAlpha(image.color, alphaDisabled);
            tokens.Add(token);
        }
    }
}
