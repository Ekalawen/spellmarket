using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class UI_Player : FastMonoBehaviour {

    public UI_Counter health;
    public UI_Counter power;
    public UI_Zone hand;
    public TMP_Text controlledByOpponentText;
    public TMP_Text hintPhaseText;

    protected Player player;

    public void Initialize(Player player)
    {
        this.player = player;
        health.Initialize(player.health);
        power.Initialize(player.power);
        hand.Initialize(player.hand);
        InitializeControlledByOpponentText();
        InitializeWhatToDoText();
    }

    private void InitializeControlledByOpponentText() {
        SetInactiveControlledByOpponentText();
    }

    public void SetActiveControlledByOpponentText() {
        controlledByOpponentText.gameObject.SetActive(true);
    }

    public void SetInactiveControlledByOpponentText() {
        controlledByOpponentText.gameObject.SetActive(false);
    }

    protected void InitializeWhatToDoText() {
        TurnSequencer playerTurn = gm.gameEngine.phaseManager.GetFirstTurnSequenceFor(player);
        LinkToPlayerTurn(playerTurn, false);
        gm.gameEngine.phaseManager.onCreateTurn.AddListener(LinkToPlayerTurn);
        SetNoHint();
    }

    protected void LinkToPlayerTurn(TurnSequencer playerTurn, bool isAdditionnalTurn) {
        if (playerTurn.player != player) {
            return;
        }
        Phase selectPhase = playerTurn.phases.First();
        Phase playPhase = playerTurn.phases.Last();
        SetColorForAdditionalTurn(isAdditionnalTurn, selectPhase);
        SetControlledByOpponentTextFor(playerTurn, selectPhase);
        selectPhase.onBegin.AddListener(SetSelectionPhaseHint);
        playPhase.onBegin.AddListener(SetPlayPhaseHint);
        playPhase.onEnd.AddListener(SetNoHint, priority: -1);
    }

    protected void SetControlledByOpponentTextFor(TurnSequencer playerTurn, Phase selectPhase) {
        if(playerTurn.IsControlledByOpponent()) {
            selectPhase.onBegin.AddListener(SetActiveControlledByOpponentText);
            playerTurn.onEnd.AddListener(SetInactiveControlledByOpponentText);
        } else {
            selectPhase.onBegin.AddListener(SetInactiveControlledByOpponentText);
        }
    }

    private void SetColorForAdditionalTurn(bool isAdditionnalTurn, Phase selectPhase) {
        if (isAdditionnalTurn) {
            selectPhase.onBegin.AddListener(SetHintTextColorForAdditionalTurn);
        } else {
            selectPhase.onBegin.AddListener(SetHintTextColorForDefaultTurn);
        }
    }

    protected void SetHintTextColorForDefaultTurn() {
        hintPhaseText.color = gm.postProcessManager.playerDefaultColor;
    }

    protected void SetHintTextColorForAdditionalTurn() {
        hintPhaseText.color = gm.postProcessManager.playerAdditionnalTurnColor;
    }

    protected void SetSelectionPhaseHint() {
        SetHint("(Choisissez une carte dans la rivi�re !)");
    }

    protected void SetPlayPhaseHint() {
        SetHint("(Jouez une carte de votre main !)");
    }

    protected void SetNoHint() {
        EndGameChecker endGameChecker = gm.gameEngine.endGameChecker;
        if(endGameChecker != null && endGameChecker.IsGameOver()) {
            return;
        }
        hintPhaseText.gameObject.SetActive(false);
    }

    public Player GetPlayer() {
        return player;
    }

    public void SetWinningHint() {
        SetHint("VOUS AVEZ GAGN� !");
    }

    public void SetLosingHint() {
        SetHint("Vous avez perdu. Demandez une revanche !");
    }

    public void SetTieHint() {
        SetHint("Vous avez fait �galit� !");
    }

    public void SetHint(string hint) {
        hintPhaseText.gameObject.SetActive(true);
        hintPhaseText.text = hint;
    }
}
