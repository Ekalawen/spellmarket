using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using MyBox;
using System;

public class GameManager : Singleton<GameManager> {

    [Header("Managers")]
    public GameObject gameEnginePrefab;
    public GameObject cameraManagerPrefab;
    public GameObject audioManagerPrefab;
    public GameObject postProcessManagerPrefab;
    public GameObject uiGamePrefab;

    [HideInInspector]
    public Game gameEngine;
    [HideInInspector]
    public CameraManager cameraManager;
    [HideInInspector]
    public AudioManager audioManager;
    [HideInInspector]
    public PostProcessManager postProcessManager;
    [HideInInspector]
    public TimerManager timerManager;
    [HideInInspector]
    public UI_Game uiGame;
    [HideInInspector]
    public InputManager inputManager;
    [HideInInspector]
    public Transform managerFolder;
    [HideInInspector]
    public bool partieDejaTerminee = false;
    protected bool isPaused = false;
    protected bool initializationIsOver = false;
    [HideInInspector]
    public UnityEvent onInitilizationFinish;
    [HideInInspector]
    public UnityEvent onFirstFrame;

    void Start()
    {
        managerFolder = transform;
        gameEngine = Instantiate(gameEnginePrefab).GetComponent<Game>();
        timerManager = TimerManager.Instance;
        timerManager.transform.SetParent(managerFolder);
        cameraManager = Instantiate(cameraManagerPrefab, managerFolder).GetComponent<CameraManager>();
        audioManager = Instantiate(audioManagerPrefab, managerFolder).GetComponent<AudioManager>();
        postProcessManager = Instantiate(postProcessManagerPrefab, managerFolder).GetComponent<PostProcessManager>();
        uiGame = Instantiate(uiGamePrefab).GetComponent<UI_Game>();
        inputManager = InputManager.Instance;

        Initialize();

        BeginGame();
    }

    protected virtual void Initialize()
    {
        gameEngine.Initialize();
        cameraManager.Initialize();
        audioManager.Initialize();
        postProcessManager.Initialize();
        uiGame.Initialize(gameEngine);
        FinishInitialization();
    }

    protected void BeginGame() {
        gameEngine.Begin();
    }

    private void FinishInitialization()
    {
        initializationIsOver = true;
        onInitilizationFinish.Invoke();
        CallEventsOneFrameAfterFinishInitialization();
    }

    protected void CallEventsOneFrameAfterFinishInitialization() {
        StartCoroutine(CCallEventsOneFrameAfterFinishInitialization());
    }

    protected IEnumerator CCallEventsOneFrameAfterFinishInitialization() {
        yield return null;
        onFirstFrame.Invoke();
    }

    void Update() {
        CheckRestartGame();

        CheckPauseToggling();
    }

    protected void CheckPauseToggling() {
        if (inputManager.GetPauseGame()) {
            if (!isPaused) {
                Pause();
            }
            else
            {
                UnPause();
            }
        }
    }

    public void Pause() {
        isPaused = true;
        Time.timeScale = 0.0f;
        audioManager.PauseSounds();
    }

    public void UnPause() {
        isPaused = false;
        Time.timeScale = 1.0f;
        audioManager.UnPauseSounds();
    }

    public bool IsPaused() {
        return isPaused;
    }

    protected void CheckRestartGame()
    {
        if (inputManager.GetRestartGame())
        {
            RestartGame();
        }
        if (inputManager.GetQuitGame())
        {
            QuitGame();
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1.0f;
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public bool IsInitializationOver()
    {
        return initializationIsOver;
    }
}


